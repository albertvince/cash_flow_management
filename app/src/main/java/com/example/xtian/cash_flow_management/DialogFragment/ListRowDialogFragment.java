package com.example.xtian.cash_flow_management.DialogFragment;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.xtian.cash_flow_management.Adapters.AvailabletoSpendAdapter;
import com.example.xtian.cash_flow_management.Adapters.CashInAdapter;
import com.example.xtian.cash_flow_management.Adapters.CashOutAdapter;
import com.example.xtian.cash_flow_management.Adapters.ReportAvailabletoSpendAdapter;
import com.example.xtian.cash_flow_management.Adapters.ReportCashInAdapter;
import com.example.xtian.cash_flow_management.Adapters.ReportCashOutAdapter;
import com.example.xtian.cash_flow_management.Models.AvailabletoSpend;
import com.example.xtian.cash_flow_management.Models.CashIn;
import com.example.xtian.cash_flow_management.Models.CashOut;
import com.example.xtian.cash_flow_management.R;

import io.realm.Realm;
import io.realm.RealmResults;

public class ListRowDialogFragment extends DialogFragment {
    private Context context;
    private Realm realm;
    private View view;
    private ListView lvRecords;
    private TextView tvTotal, tvTax;
    private LinearLayout linearLayoutHeader;
    private ReportCashInAdapter reportCashInAdapter;
    private ReportCashOutAdapter reportCashOutAdapter;
    private ReportAvailabletoSpendAdapter reportAvailabletoSpendAdapter;
    private View emptyIndicator;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.dialog_fragment_list_row, container, false);
        context = view.getContext();
        realm = Realm.getDefaultInstance();
        initializeUI();
        Bundle bundle = getArguments();
        if (getArguments() != null)
        {
            if (bundle.getString("TAG").equals("CASHIN")){
                readCashIn(bundle.getString("BATCHID"));
            }
            else if (bundle.getString("TAG").equals("CASHOUT")){
                readCashOut(bundle.getString("BATCHID"));
            }
            else if (bundle.getString("TAG").equals("AVAILABLETOSPEND")){
                readAvailabletoSpend(bundle.getString("BATCHID"));
            }
        }

        return view;
    }

    private void initializeUI(){
        linearLayoutHeader = view.findViewById(R.id.linearLayoutHeader);
        lvRecords = view.findViewById(R.id.lvRecords);
        emptyIndicator = view.findViewById(R.id.viewEmptyListIndicator);
        tvTax = view.findViewById(R.id.tvTax);
        tvTotal = view.findViewById(R.id.tvTotal);
    }


    private void readCashIn(String search) {
        RealmResults<CashIn> cashInRealmResults = realm.where(CashIn.class).equalTo
                ("batchID", search).findAll();
        cashInRealmResults.load();

        reportCashInAdapter = new ReportCashInAdapter(context, cashInRealmResults);
        lvRecords.setAdapter(reportCashInAdapter);

        reportCashInAdapter.notifyDataSetChanged();
        showEmptyListIndicator(cashInRealmResults.size() <= 0);
    }

    private void readCashOut(String search) {
        RealmResults<CashOut> cashOutRealmResults = realm.where(CashOut.class).equalTo
                ("batchID", search).findAll();
        cashOutRealmResults.load();

        reportCashOutAdapter = new ReportCashOutAdapter(context, cashOutRealmResults);
        lvRecords.setAdapter(reportCashOutAdapter);

        reportCashOutAdapter.notifyDataSetChanged();
        showEmptyListIndicator(cashOutRealmResults.size() <= 0);
    }

    private void readAvailabletoSpend(String search) {
        RealmResults<AvailabletoSpend> availabletoSpendRealmResults = realm.where(AvailabletoSpend.class).equalTo
                ("batchID", search).findAll();
        availabletoSpendRealmResults.load();

        reportAvailabletoSpendAdapter = new ReportAvailabletoSpendAdapter(context, availabletoSpendRealmResults);
        lvRecords.setAdapter(reportAvailabletoSpendAdapter);

        reportAvailabletoSpendAdapter.notifyDataSetChanged();
        showEmptyListIndicator(availabletoSpendRealmResults.size() <= 0);
        tvTotal.setVisibility(View.GONE);
        tvTax.setVisibility(View.GONE);
    }

//    private void readCashOut(String search) {
//        RealmResults<CashOut> cashOutRealmResults = realm.where(CashOut.class).equalTo
//                ("batchID", search).findAll();
//        cashOutRealmResults.load();
//
//        cashOutAdapter = new CashOutAdapter(context, cashOutRealmResults);
//        lvRecords.setAdapter(cashOutAdapter);
//
//        cashOutAdapter.notifyDataSetChanged();
//        showEmptyListIndicator(cashOutRealmResults.size() <= 0);
//        tvDialogName.setText("Cash Out");
//    }
//
//    private void readAvailabletoSpend(String search) {
//        RealmResults<AvailabletoSpend> availabletoSpendRealmResults = realm.where(AvailabletoSpend.class).equalTo
//                ("batchID", search).findAll();
//        availabletoSpendRealmResults.load();
//
//        availabletoSpendAdapter = new AvailabletoSpendAdapter(context, availabletoSpendRealmResults);
//        lvRecords.setAdapter(availabletoSpendAdapter);
//
//        availabletoSpendAdapter.notifyDataSetChanged();
//        showEmptyListIndicator(availabletoSpendRealmResults.size() <= 0);
//        tvDialogName.setText("Available to Spend");
//
//    }

    private void showEmptyListIndicator(boolean show)
    {
        emptyIndicator.setVisibility(show ? View.VISIBLE : View.GONE);
        linearLayoutHeader.setVisibility(show ? View.GONE : View.VISIBLE);
    }
}
