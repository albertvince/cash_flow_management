package com.example.xtian.cash_flow_management.Activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.example.xtian.cash_flow_management.MainActivity;
import com.example.xtian.cash_flow_management.R;
import com.example.xtian.cash_flow_management.Utils.Debugger;
import com.example.xtian.cash_flow_management.Utils.HttpProvider;
import com.example.xtian.cash_flow_management.Utils.UserSession;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import es.dmoral.toasty.Toasty;

public class RegisterActivity extends AppCompatActivity {

    private EditText etEmail, etPassword, etConfirmPassword;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        getSupportActionBar().hide();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        context = this;
    }

    @Override
    protected void onStart() {
        super.onStart();
        initializeUI();
    }

    private void initializeUI()
    {

        Button btnSubmit;

        etEmail = (EditText) findViewById(R.id.etEmail);
        etPassword = (EditText) findViewById(R.id.etPass);
        etConfirmPassword = (EditText) findViewById(R.id.etConfirmPass);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                register();
            }
        });
    }

    private void register()
    {
        try
        {
            JSONObject jsonObject = new JSONObject();
            final ProgressDialog progressDialog = new ProgressDialog(context);
            jsonObject.put("email", etEmail.getText().toString());
            jsonObject.put("password", etPassword.getText().toString());
            jsonObject.put("confirm_password", etConfirmPassword.getText().toString());

            StringEntity stringEntity = new StringEntity(jsonObject.toString());

            HttpProvider.post(context, "/api/cashflow/register/", stringEntity, "application/json", new JsonHttpResponseHandler(){

                @Override
                public void onStart() {
                    super.onStart();
                    progressDialog.show();
                }

                @Override
                public void onFinish() {
                    super.onFinish();
                    progressDialog.dismiss();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode, headers, response);
                    Debugger.logD(response.toString());
                    Toasty.success(context, "Success").show();
                    UserSession session = new Gson().fromJson(response.toString(), new TypeToken<UserSession>(){}.getType());
                    session.saveUserSession(getApplicationContext());
                    Intent main_intent = new Intent(getApplicationContext(), MainActivity.class);
                    main_intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(main_intent);
                    finish();


                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                    super.onSuccess(statusCode, headers, response);
                    Debugger.logD(response.toString());
                    Toasty.success(context, "Success").show();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    Toasty.error(context, "Error From Server 1").show();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    Toasty.error(context, "Error from Server 2").show();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    Toasty.error(context, responseString).show();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    super.onSuccess(statusCode, headers, responseString);
                }
            });

        } catch (Exception err)
        {
            Toasty.error(context, err.toString()).show();
        }
    }
}
