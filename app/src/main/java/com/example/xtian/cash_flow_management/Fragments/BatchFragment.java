package com.example.xtian.cash_flow_management.Fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;


import com.example.xtian.cash_flow_management.Activities.CreateBatchActivity;
import com.example.xtian.cash_flow_management.Activities.ViewBatchActivity;
import com.example.xtian.cash_flow_management.Adapters.BatchAdapter;
import com.example.xtian.cash_flow_management.DialogFragment.CurrencyDialogFragment;
import com.example.xtian.cash_flow_management.DialogFragment.TimeFrameDialogFragment;
import com.example.xtian.cash_flow_management.Interface.IDialogCloseInterface;
import com.example.xtian.cash_flow_management.MainActivity;
import com.example.xtian.cash_flow_management.Models.AvailabletoSpend;
import com.example.xtian.cash_flow_management.Models.Batch;
import com.example.xtian.cash_flow_management.R;
import com.example.xtian.cash_flow_management.Utils.Debugger;
import com.example.xtian.cash_flow_management.Utils.UserSession;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import es.dmoral.toasty.Toasty;
import io.realm.OrderedCollectionChangeSet;
import io.realm.OrderedRealmCollectionChangeListener;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;
import uk.co.deanwild.materialshowcaseview.ShowcaseConfig;

public class BatchFragment extends Fragment implements IDialogCloseInterface.onDismissDialogListener {
    private Realm realm;
    private RealmResults<Batch> batchRealmResults;
    private RealmChangeListener realmChangeListener;
    private View view;
    private Context context;
    private Batch batch;
    private ListView lvBatches;
    private FloatingActionButton floatingActionButton;
    private Batch selectedBatch;
    private View emptyIndicator;
    private int nextId;
    private BatchAdapter batchAdapter;
    private boolean isComplete;
    private Date dateModified;
    private UserSession session;
    private Spinner spinnerBatch;
    private String selectedCanvasType;
    private List<String> canvasType;
    private List<String> staticAvailabletoSpendData;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_batch, container, false);
        context = getContext();
        setPageTitle("Transactions");
        realm = Realm.getDefaultInstance();
        return view;
    }


    @SuppressLint("RestrictedApi")
    @Override
    public void onStart() {
        super.onStart();
        batch = new Batch();
        initializeUI();
        registerForContextMenu(lvBatches);

    }

    private void initializeUI() {
        floatingActionButton = ((MainActivity) getActivity()).getFloatingActionButton();
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() == null) return;
                checkTimeFrame();
            }
        });
        lvBatches = (ListView) view.findViewById(R.id.lvBatches);
        emptyIndicator = (View) view.findViewById(R.id.viewEmptyListIndicator);
        canvasType = new ArrayList<String>();
        spinnerBatch = view.findViewById(R.id.spinnerBatch);
        staticAvailabletoSpendData = new ArrayList<String>();
        populateBatch();
        ArrayAdapter<String> canvasTypeAdapter = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item, canvasType);
        spinnerBatch.setAdapter(canvasTypeAdapter);
        spinnerBatch.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedCanvasType = spinnerBatch.getItemAtPosition(position).toString();
                setCanvasType(selectedCanvasType);
                readRecords();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        setDefault();
        readRecords();
        lvBatches.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                selectedBatch = batchAdapter.getItem(position);
                lvBatches.showContextMenu();
            }
        });

        lvBatches.setDivider(null);
        loadSettings();
    }

    private void loadSettings(){
        if (session.getCurrency(context).equals("")){
            openCurrencyDialog();
        }
    }

    private void openTimeFrameDialog(){
        TimeFrameDialogFragment timeFrameDialogFragment = new TimeFrameDialogFragment();
        timeFrameDialogFragment.setTargetFragment(this, 356);
        timeFrameDialogFragment.show(getFragmentManager(), "TIME_FRAME");
    }

    private void openCurrencyDialog(){
        CurrencyDialogFragment currencyDialogFragment = new CurrencyDialogFragment();
        currencyDialogFragment.setTargetFragment(this, 540);
        currencyDialogFragment.show(getFragmentManager(), "CURRENCY");
    }

    private void checkUserGuide(){
        if (session.getUserGuide(context) == 0){
            loadInstruction1();
        }
        if (session.getUserGuide(context) == 7){
//            loadInstruction8();
        }
    }

    private void loadInstruction1(){
       new MaterialShowcaseView.Builder(getActivity()).setTarget(floatingActionButton)
               .setDismissText("Got it")
               .setContentText("Step 1 - To start a transaction, click this button here.")
               .setDelay(500)
               .show();
        setUserGuide(1);
    }
//
//    private void loadInstruction8(){
//        new MaterialShowcaseView.Builder(getActivity()).setTarget(spinnerBatch)
//                .setDismissText("Got it")
//                .setContentText("Step 8 - You can also select a transaction canvas type")
//                .setDelay(500)
//                .show();
//        setUserGuide(8);
//    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId() == R.id.lvBatches)
        {
            MenuInflater inflater = getActivity().getMenuInflater();
            inflater.inflate(R.menu.selected_item_menu, menu);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.edit:
                openCreateBatchActivity();
                return true;
            case R.id.remove:
                deleteRecord();
                return true;
            case R.id.view:
                viewRecord();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    private void openCreateBatchActivity() {
        Intent intent = new Intent(view.getContext(), CreateBatchActivity.class);
        intent.putExtra("BATCH", selectedBatch);
        startActivity(intent);
    }

    private void viewRecord() {
        Intent intent = new Intent(view.getContext(), ViewBatchActivity.class);
        intent.putExtra("VIEWBATCH", selectedBatch);
        startActivity(intent);
    }

    private void readRecords() {
        batchRealmResults = realm.where(Batch.class).equalTo("canvasType", selectedCanvasType)
                .findAll();
        batchRealmResults.load();

        batchAdapter = new BatchAdapter(context, batchRealmResults, getActivity(), getFragmentManager());


        OrderedRealmCollectionChangeListener<RealmResults<Batch>> callback = new OrderedRealmCollectionChangeListener<RealmResults<Batch>>() {
            @Override
            public void onChange(RealmResults<Batch> batches, OrderedCollectionChangeSet changeSet) {
                if (batchAdapter != null){
                    batchAdapter.notifyDataSetChanged();
                }
                showEmptyListIndicator(batchRealmResults.size() <= 0);
            }
        };

        batchRealmResults.addChangeListener(callback);
        lvBatches.setAdapter(batchAdapter);

        showEmptyListIndicator(batchRealmResults.size() <= 0);
    }

    private void updateRecords(){
        batchRealmResults = realm.where(Batch.class).equalTo("canvasType", selectedCanvasType)
                .findAll();
        batchRealmResults.load();
        batchAdapter = new BatchAdapter(context, batchRealmResults, getActivity(), getFragmentManager());

        lvBatches.setAdapter(batchAdapter);

        showEmptyListIndicator(batchRealmResults.size() <= 0);
    }

    @Override
    public void onResume() {
        super.onResume();
        checkUserGuide();
    }

    private void setUserGuide(int guide){
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt("USER_GUIDE", guide);
        editor.apply();
    }

    private void setDefault(){
        if (session.getCanvasType(context).equals("Review")){
            spinnerBatch.setSelection(0);
        }
        else if(session.getCanvasType(context).equals("Forecast")){
            spinnerBatch.setSelection(1);
        }
        else if(session.getCanvasType(context).equals("Plan")){
            spinnerBatch.setSelection(2);
        }
        else if (session.getCanvasType(context).equals("Track")){
            spinnerBatch.setSelection(3);
        }
    }

    private void deleteRecord() {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<Batch> result = realm.where(Batch.class).equalTo("uuid", selectedBatch.getUuid()).findAll();
                result.deleteAllFromRealm();
                readRecords();
            }
        });
    }

    private void showEmptyListIndicator(boolean show)
    {
        emptyIndicator.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    private void saveToDB() {
        setData();
        Batch batch = null;
        realm.beginTransaction();
        if (batch == null) {
            Batch newBatch = realm.createObject(Batch.class, UUID.randomUUID().toString());
            selectedBatch = newBatch;
            nextId = getNextKey(newBatch);
            setFields(newBatch);
        }
        realm.commitTransaction();
        setAvailabletoSpendStaticData();
        readRecords();
    }

    private int getNextKey(Batch batch) {
        try {
            Number number = realm.where(Batch.class).max("batchCode");
            if (number != null){
                return number.intValue() + 1;
            } else {
                return 1;
            }
        } catch (ArrayIndexOutOfBoundsException e){
            return 1;
        }
    }

    private void setData() {
        //Date Today
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        calendar.add(Calendar.DATE, 30);
        isComplete = false;
        dateModified = date;
    }

    private void setFields(Batch batch) {
        String strIsComplete = String.valueOf(isComplete);
        batch.setDateModified(dateModified);
        batch.setIsComplete(strIsComplete);
        batch.setBatchCode(nextId);
        batch.setTimeFrame(session.getTimeFrame(context));
        batch.setCanvasType(session.getCanvasType(context));
        batch.setUserID(UserSession.getUserId(context));
    }

    private void setAvailabletoSpendStaticData(){
        setArray();
        for (int i = 0; i < staticAvailabletoSpendData.size(); i++){
            realm.beginTransaction();
            AvailabletoSpend availabletoSpend = realm.createObject(AvailabletoSpend.class, UUID.randomUUID().toString());
            availabletoSpend.setAccountTitle(staticAvailabletoSpendData.get(i));
            availabletoSpend.setAccountAmount(0.00);
            availabletoSpend.setDateModified(dateModified);
            availabletoSpend.setBatchID(selectedBatch.getUuid());
            realm.commitTransaction();
        }

    }

    private void setArray(){
        staticAvailabletoSpendData.add("Cash to Owner");
        staticAvailabletoSpendData.add("Cash at Bank");
        staticAvailabletoSpendData.add("Stock");
        staticAvailabletoSpendData.add("Debtors");
        staticAvailabletoSpendData.add("Creditors");
        staticAvailabletoSpendData.add("Assets");
        staticAvailabletoSpendData.add("Loans");
    }


    public void setPageTitle(String title) {
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(Html.fromHtml("<font color='#ffffff'>"+title+ "</font>"));
    }

    private void checkTimeFrame(){
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();

        Batch batch = realm.where(Batch.class).equalTo("canvasType", session.getCanvasType(context)).findAll().last(null);

        if (batch == null){
            saveToDB();
            openCreateBatchActivity();
        }
        else {

            Calendar newCalendar = Calendar.getInstance();
            newCalendar.setTime(batch.getDateModified());
            if (session.getTimeFrame(context).equals("Weekly")){
                newCalendar.add(Calendar.DATE, 7);
            }
            else if (session.getTimeFrame(context).equals("Monthly")){
                newCalendar.add(Calendar.MONTH, 1);
            }
            else if (session.getTimeFrame(context).equals("Quarterly")){
                newCalendar.add(Calendar.MONTH, 4);
            }
            else if (session.getTimeFrame(context).equals("Yearly")){
                newCalendar.add(Calendar.YEAR, 1);
            }
            else {
                newCalendar.add(Calendar.DATE, 7);
            }
            Date batchDate = newCalendar.getTime();
            if (date.compareTo(batchDate) <= 0){
                Toasty.info(context, "You have already added your " + session.getTimeFrame(context) + " " + session.getCanvasType(context) + " entry").show();
            }
            else {
                saveToDB();
                openCreateBatchActivity();
            }
        }
    }

    private void populateBatch(){
        canvasType.add("Review");
        canvasType.add("Forecast");
        canvasType.add("Plan");
        canvasType.add("Track");
    }

    private void setCanvasType(String selectedCanvasType){
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("CANVAS_TYPE", selectedCanvasType);
        editor.apply();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        batchRealmResults.removeAllChangeListeners();
        realm.close();
    }

    @Override
    public void onDissmissDialog() {
        if (session.getTimeFrame(context) != null){
            if (session.getCurrency(context) == null){
                openCurrencyDialog();
            }
        }
        if (session.getCurrency(context) != null){
            if (session.getTimeFrame(context) == null){
                openTimeFrameDialog();
            }
        }
    }
}


