package com.example.xtian.cash_flow_management.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.example.xtian.cash_flow_management.Models.CashIn;
import com.example.xtian.cash_flow_management.R;
import com.example.xtian.cash_flow_management.Utils.Converter;
import com.example.xtian.cash_flow_management.Utils.DateTimeHandler;
import com.example.xtian.cash_flow_management.Utils.Debugger;
import com.example.xtian.cash_flow_management.Utils.UserSession;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import io.realm.RealmResults;

public class CashInAdapter extends ArrayAdapter<CashIn> implements ListAdapter {

    private final int mResource;
    private int position;
    private Context mContext;
    private List<CashIn> readingList = new ArrayList<>();
    private UserSession session;

    public CashInAdapter(@NonNull Context context, RealmResults<CashIn> list) {
        super(context, 0 , list);
        mContext = context;
        mResource = 0;
        readingList = list;
    }



    @Override
    public int getPosition(@Nullable CashIn item)
    {
        int x=0;
        return super.getPosition(item);
    }
    public void setPosition(int _position)
    {
        position = _position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return createViewFromResource(LayoutInflater.from(getContext()), position, convertView, parent, mResource);
    }

    private @NonNull
    View createViewFromResource(@NonNull LayoutInflater inflater, int position,
                                @Nullable View convertView, @NonNull ViewGroup parent, int resource)
    {
        final TextView accountTitle;
        final TextView amount;
        final TextView date;
        CashIn cashIn = readingList.get(position);

        if(convertView == null) {
            convertView= LayoutInflater.from(mContext).inflate(R.layout.list_row_cash_in, parent, false);
        }

        accountTitle =  convertView.findViewById(R.id.tvAccountTitle);
        amount =  convertView.findViewById(R.id.tvAmount);
        date = convertView.findViewById(R.id.tvDate);

        accountTitle.setText(cashIn.getAccountTitle());

        DecimalFormat decimalFormat = new DecimalFormat("#,###.00");
        String numberasString = decimalFormat.format(cashIn.getAccountAmount() + cashIn.getVAT());
        amount.setText(session.getCurrency(mContext) + numberasString);
        date.setText(DateTimeHandler.convertDatetoMonthWord(cashIn.getDateModified()));

        return convertView;
    }
}
