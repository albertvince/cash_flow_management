package com.example.xtian.cash_flow_management.DialogFragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.xtian.cash_flow_management.Fragments.BatchFragment;
import com.example.xtian.cash_flow_management.Fragments.SettingsFragment;
import com.example.xtian.cash_flow_management.R;

public class CurrencyDialogFragment extends DialogFragment {
    private Context context;
    private View view;
    private CardView cvPh, cvChina, cvUS, cvSK;
    private TextView tvPeso, tvYen, tvUSD, tvWon, tvCountryPh, tvCountryUS, tvCountryChina, tvCountrySK;
    private String selectedCurrency, selectedCountry;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.dialog_list_row_currency, container, false);
        context = view.getContext();
        initializeUI();
        return view;
    }


    private void initializeUI(){
        tvPeso = view.findViewById(R.id.tvPeso);
        tvYen = view.findViewById(R.id.tvYen);
        tvUSD = view.findViewById(R.id.tvUSD);
        tvWon = view.findViewById(R.id.tvWon);
        tvCountryPh = view.findViewById(R.id.tvCountryPh);
        tvCountryUS = view.findViewById(R.id.tvCountryUS);
        tvCountryChina = view.findViewById(R.id.tvCountryChina);
        tvCountrySK = view.findViewById(R.id.tvCountrySK);

        cvPh = view.findViewById(R.id.cvPhilippines);
        cvPh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedCurrency = tvPeso.getText().toString();
                selectedCountry = tvCountryPh.getText().toString();
                saveCurrency(selectedCurrency, selectedCountry);
                dismiss();
            }
        });

        cvChina = view.findViewById(R.id.cvChina);
        cvChina.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedCurrency = tvYen.getText().toString();
                selectedCountry = tvCountryChina.getText().toString();
                saveCurrency(selectedCurrency, selectedCountry);
                dismiss();
            }
        });

        cvUS = view.findViewById(R.id.cvUS);
        cvUS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedCurrency = tvUSD.getText().toString();
                selectedCountry = tvCountryUS.getText().toString();
                saveCurrency(selectedCurrency, selectedCountry);
                dismiss();
            }
        });

        cvSK = view.findViewById(R.id.cvSouthKorea);
        cvSK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedCurrency = tvWon.getText().toString();
                selectedCountry = tvCountrySK.getText().toString();
                saveCurrency(selectedCurrency, selectedCountry);
                dismiss();
            }
        });
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        if (getTargetRequestCode() == 130){
            ((SettingsFragment)getTargetFragment()).onDissmissDialog();
        }
        else if (getTargetRequestCode() == 540){
            ((BatchFragment)getTargetFragment()).onDissmissDialog();
        }
    }

    private void saveCurrency(String selectedCurrency, String selectedCountry){
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("CURRENCY", selectedCurrency);
        editor.putString("COUNTRY_NAME", selectedCountry);
        editor.apply();
    }
}
