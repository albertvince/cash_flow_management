package com.example.xtian.cash_flow_management.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.xtian.cash_flow_management.Fragments.AvailabletoSpendFragment;
import com.example.xtian.cash_flow_management.Fragments.CashInFragment;
import com.example.xtian.cash_flow_management.Fragments.CashOutFragment;
import com.example.xtian.cash_flow_management.Fragments.CashSurplusFragment;
import com.example.xtian.cash_flow_management.Fragments.OpeningBalanceFragment;

public class PageAdapter extends FragmentPagerAdapter {

    private int numOfTabs;

    public PageAdapter(FragmentManager fm, int numOfTabs) {
        super(fm);
        this.numOfTabs = numOfTabs;
    }

    @Override
    public Fragment getItem(int i) {
        switch(i) {
            case 0 :
                return new OpeningBalanceFragment();
            case 1 :
                return new CashInFragment();
            case 2 :
                return new CashOutFragment();
            case 3 :
                return new CashSurplusFragment();
            case 4 :
                return new AvailabletoSpendFragment();
            default:
                return new OpeningBalanceFragment();
        }
    }

    @Override
    public int getCount() { return numOfTabs; }
}
