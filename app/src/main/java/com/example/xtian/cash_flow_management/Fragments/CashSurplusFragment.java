package com.example.xtian.cash_flow_management.Fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.xtian.cash_flow_management.Activities.CreateBatchActivity;
import com.example.xtian.cash_flow_management.Models.CashIn;
import com.example.xtian.cash_flow_management.Models.CashOut;
import com.example.xtian.cash_flow_management.Models.OpeningBalance;
import com.example.xtian.cash_flow_management.R;
import com.example.xtian.cash_flow_management.Utils.Debugger;
import com.example.xtian.cash_flow_management.Utils.UserSession;

import java.text.DecimalFormat;
import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

public class CashSurplusFragment extends Fragment{
    private Realm realm;
    private View view;
    private Context context;
    private TextView tvAmount, tvTotalExcludeLabel;
    private TextView tvTutorialTotal;
    private String batchID;
    private UserSession session;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_cash_surplus, container,false);
        context = getContext();
        realm = Realm.getDefaultInstance();
        return view;

    }

    public void onStart() {
        super.onStart();
        initializeUI();
    }

    public static CashSurplusFragment newInstance() {
        CashSurplusFragment fragment = new CashSurplusFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @SuppressLint("RestrictedApi")
    private void initializeUI() {
        tvAmount = view.findViewById(R.id.tvAmount);
        batchID = ((CreateBatchActivity)getActivity()).getCurrentBatchID();
        tvTotalExcludeLabel = view.findViewById(R.id.tvTotalExcludeLabel);
        readSurplusAmount();
        readLiveData(batchID);
        tvTutorialTotal = getActivity().findViewById(R.id.tvAmount);

        if (session.getTaxTerms(context) != null){
            tvTotalExcludeLabel.setText("Total excluding " + session.getTaxTerms(context));
        }
        else {
            tvTotalExcludeLabel.setText("Total excluding Tax");
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        readLiveData(batchID);
    }



    private void readLiveData(String search){
        RealmResults<OpeningBalance> openingBalanceRealmResults = realm.where(OpeningBalance.class).equalTo
                ("batchID", search).findAll();
        openingBalanceRealmResults.load();

        openingBalanceRealmResults.addChangeListener(new RealmChangeListener<RealmResults<OpeningBalance>>() {
            @Override
            public void onChange(RealmResults<OpeningBalance> cashIns) {
                readSurplusAmount();

            }
        });

        RealmResults<CashIn> cashInRealmResults = realm.where(CashIn.class).equalTo
                ("batchID", search).findAll();
        cashInRealmResults.load();

        cashInRealmResults.addChangeListener(new RealmChangeListener<RealmResults<CashIn>>() {
            @Override
            public void onChange(RealmResults<CashIn> cashIns) {
                readSurplusAmount();
                Debugger.logD("Naay Update cashinSurpulus");
            }
        });

        RealmResults<CashOut> cashOutRealmResults = realm.where(CashOut.class).equalTo
                ("batchID", search).findAll();
        cashOutRealmResults.load();
        cashOutRealmResults.addChangeListener(new RealmChangeListener<RealmResults<CashOut>>() {
            @Override
            public void onChange(RealmResults<CashOut> cashOuts) {
                readSurplusAmount();
                Debugger.logD("Naay Update cashoutSurplus");
            }
        });
    }

    private Double readCashInTotalAmount(String search) {
        Double totalAmount = 0.0;

        RealmResults<CashIn> cashInRealmResults = realm.where(CashIn.class).equalTo
                ("batchID", search).findAll();
        ArrayList<CashIn> cashInArrayList = new ArrayList<>(cashInRealmResults);

        for (CashIn cashIn : cashInArrayList) {
            CashIn total = realm.where(CashIn.class).equalTo("accountAmount", cashIn.getAccountAmount()).findFirst();
            totalAmount += total.getAccountAmount();
        }
        return totalAmount;
    }

    private Double readCashOutTotalAmount(String search) {
        Double totalAmount = 0.0;

        RealmResults<CashOut> cashOutRealmResults = realm.where(CashOut.class).equalTo
                ("batchID", search).findAll();
        ArrayList<CashOut> cashOutArrayList = new ArrayList<>(cashOutRealmResults);

        for (CashOut cashOut : cashOutArrayList) {
            CashOut total = realm.where(CashOut.class).equalTo("accountAmount", cashOut.getAccountAmount()).findFirst();
            totalAmount += total.getAccountAmount();
        }
        return totalAmount;
    }

    private Double readOpeningBalance(String search) {
        Double totalAmount = 0.0;

        RealmResults<OpeningBalance> openingBalanceRealmResults = realm.where(OpeningBalance.class).equalTo
                ("batchID", search).findAll();
        ArrayList<OpeningBalance> openingBalanceArrayList = new ArrayList<>(openingBalanceRealmResults);

        for (OpeningBalance openingBalance : openingBalanceArrayList) {
            OpeningBalance total = realm.where(OpeningBalance.class).equalTo("amount", openingBalance.getAmount()).findFirst();
            totalAmount += total.getAmount();
        }

        return totalAmount;
    }

    private void readSurplusAmount() {
        Double cashInTotal, cashOutTotal, openingBalance, cashSurplus;
        cashInTotal = readCashInTotalAmount(batchID);
        cashOutTotal = readCashOutTotalAmount(batchID);
        openingBalance = readOpeningBalance(batchID);

        cashSurplus = (openingBalance + cashInTotal) - cashOutTotal;

        DecimalFormat decimalFormat = new DecimalFormat("#,###.00");
        String numberasString = decimalFormat.format(cashSurplus);

        if (cashSurplus == 0){
            tvAmount.setText(session.getCurrency(context) + "0.00");
        }
        else {
            tvAmount.setText(session.getCurrency(context) + numberasString);
        }
    }

}
