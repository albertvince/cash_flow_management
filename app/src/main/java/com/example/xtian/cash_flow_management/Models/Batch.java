package com.example.xtian.cash_flow_management.Models;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.xtian.cash_flow_management.Utils.DateTimeHandler;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

public class Batch extends RealmObject implements Parcelable {

    @PrimaryKey
    @Required
    private String uuid;
    private Date dateModified;
    private String isComplete;
    private Integer batchCode;
    private String timeFrame;
    private String canvasType;

    @Index
    private int userID;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Date getDateModified() {
        return dateModified;
    }

    public void setDateModified(Date dateModified) {
        this.dateModified = dateModified;
    }

    public String getIsComplete() {
        return isComplete;
    }

    public void setIsComplete(String isComplete) {
        this.isComplete = isComplete;
    }

    public Integer getBatchCode() {
        return batchCode;
    }

    public void setBatchCode(Integer batchCode) {
        this.batchCode = batchCode;
    }

    public String getTimeFrame() {
        return timeFrame;
    }

    public void setTimeFrame(String timeFrame) {
        this.timeFrame = timeFrame;
    }


    public String getCanvasType() {
        return canvasType;
    }

    public void setCanvasType(String canvasType) {
        this.canvasType = canvasType;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public Batch(){

    }

    public static final Creator<Batch> CREATOR = new Creator<Batch>() {
        @Override
        public Batch createFromParcel(Parcel in) {
            return new Batch(in);
        }

        @Override
        public Batch[] newArray(int size) {
            return new Batch[size];
        }
    };

    protected Batch(Parcel in) {
        setUuid(in.readString());
        setDateModified(DateTimeHandler.convertStringtoDate(in.readString()));
        setIsComplete(in.readString());
        setBatchCode(in.readInt());
        setTimeFrame(in.readString());
        setCanvasType(in.readString());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(getUuid());
        parcel.writeString(DateTimeHandler.convertDatetoStringDate(getDateModified()));
        parcel.writeString(getIsComplete());
        parcel.writeInt(getBatchCode());
        parcel.writeString(getTimeFrame());
        parcel.writeString(getCanvasType());
    }

}
