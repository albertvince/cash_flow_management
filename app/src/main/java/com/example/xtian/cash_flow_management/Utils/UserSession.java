package com.example.xtian.cash_flow_management.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.example.xtian.cash_flow_management.Models.User;
import com.google.gson.annotations.SerializedName;

public class UserSession {

    @SerializedName("first_name")
    private String firstName;
    @SerializedName("last_name")
    private String lastName;
    @SerializedName("contact_number")
    private String contactNumber;
    @SerializedName("company_name")
    private String companyName;
    @SerializedName("token")
    private String authToken;
    private String email;
    private String timeFrame;
    private String canvasType;
    private int autoSync;
    private int vatRate;
    private int vatAmount;
    private String taxTerms;
    private String currency;
    private String countryName;
    private int userGuide;
    private User user = new User();

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public int getUserGuide() {
        return userGuide;
    }

    public void setUserGuide(int userGuide) {
        this.userGuide = userGuide;
    }


    public class User {

        public int id;
        public String email;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }
    }

    public String getTaxTerms() {
        return taxTerms;
    }

    public void setTaxTerms(String taxTerms) {
        this.taxTerms = taxTerms;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAuthToken() {
        return authToken;
    }

    public String getTimeFrame() {
        return timeFrame;
    }

    public void setTimeFrame(String timeFrame) {
        this.timeFrame = timeFrame;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public static String getToken(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("AUTHTOKEN", null);
    }

    public static String getFirstName(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("FIRST_NAME", "");
    }

    public static String getLastName(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("LAST_NAME", "");
    }

    public static String getContactNumber(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("CONTACT_NUMBER", "");
    }

    public static String getCompany(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("COMPANY_NAME", "");
    }

    public static String getCompanyEmail(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("COMPANY_EMAIL", "");
    }

    public static String getProfilePicture(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("PROFILE_PICTURE", "");
    }

    public static int getEmployeeIdDigits(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getInt("EMP_ID_DIGITS", 0);
    }

    public static boolean clearSession(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.clear();
        return editor.commit();
    }

    public static int getUserId(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getInt("USER_ID", 0);
    }

    public static String getTimeFrame(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("TIME_FRAME", "");
    }

    public static String getUserEmail(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("COMPANY_EMAIL", "");
    }

    public static int getAutoSync(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getInt("AUTO_SYNC", 0);
    }

    public static int getVatRate(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getInt("VAT_RATE", 0);
    }

    public static String getCanvasType(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("CANVAS_TYPE", "");
    }

    public static int getVatAmount(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getInt("VAT_AMOUNT", 0);
    }

    public static String getTaxTerms(Context context){
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("TAX_TERM", "");
    }

    public static String getCurrency(Context context){
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("CURRENCY", "");
    }

    public static String getCountryName(Context context){
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("COUNTRY_NAME", "");
    }

    public static int getUserGuide(Context context){
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getInt("USER_GUIDE", 0);
    }


    public int getAutoSync() {
        return autoSync;
    }

    public void setAutoSync(int autoSync) {
        this.autoSync = autoSync;
    }

    public int getVatRate() {
        return vatRate;
    }

    public void setVatRate(int vatRate) {
        this.vatRate = vatRate;
    }

    public String getCanvasType() {
        return canvasType;
    }

    public void setCanvasType(String canvasType) {
        this.canvasType = canvasType;
    }

    public boolean saveUserSession(Context context) {
        Debugger.printO(this);
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("COMPANY_NAME", companyName);
        editor.putString("AUTHTOKEN", getAuthToken());
        editor.putString("COMPANY_EMAIL", user.getEmail());
        editor.putInt("USER_ID", user.getId());
        editor.putString("TIME_FRAME", getTimeFrame());
        editor.putString("CANVAS_TYPE", getCanvasType());
        editor.putInt("AUTO_SYNC", getAutoSync());
        editor.putInt("VAT_RATE", getVatRate());
        editor.putInt("VAT_AMOUNT", getVatAmount());
        editor.putString("TAX_TERM", getTaxTerms());
        editor.putString("CURRENCY", getCurrency());
        editor.putString("COUNTRY_NAME", getCountryName());
        editor.putInt("USER_GUIDE", getUserGuide());
        return editor.commit();
    }

    public int getVatAmount() {
        return vatAmount;
    }

    public void setVatAmount(int vatAmount) {
        this.vatAmount = vatAmount;
    }


}

