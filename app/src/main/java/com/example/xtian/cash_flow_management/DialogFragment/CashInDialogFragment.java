package com.example.xtian.cash_flow_management.DialogFragment;

import android.support.v4.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.example.xtian.cash_flow_management.Activities.CreateBatchActivity;
import com.example.xtian.cash_flow_management.Fragments.CashInFragment;
import com.example.xtian.cash_flow_management.Models.AvailabletoSpend;
import com.example.xtian.cash_flow_management.Models.CashIn;
import com.example.xtian.cash_flow_management.R;
import com.example.xtian.cash_flow_management.Utils.Converter;
import com.example.xtian.cash_flow_management.Utils.DateTimeHandler;
import com.example.xtian.cash_flow_management.Utils.Debugger;
import com.example.xtian.cash_flow_management.Utils.UserSession;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import es.dmoral.toasty.Toasty;
import io.realm.Realm;

public class CashInDialogFragment extends DialogFragment {
    private Context context;
    private View view;
    private Realm realm;
    private Button btnSave;
    private EditText etAccountTitle, etAmount;
    private TextView tvAmount, tvTax, tvTotal, tvVatType;
    private Switch switchVAT;
    private String batchID;
    private String accountTitle;
    private Double accountAmount;
    private Date dateModified;
    private LinearLayout linearLayoutVAT;
    private String selectedCashIn;
    private UserSession session;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.dialog_fragmen_add_amount, container, false);
        context = view.getContext();
        realm = Realm.getDefaultInstance();

        if (getArguments() != null)
        {
            Bundle bundle = getArguments();
            selectedCashIn = bundle.getString("TAG");
            initializeUI();
        }

        return view;
    }

    private void initializeUI() {
        TextView tvFragmentName = view.findViewById(R.id.tvFragmentName);
        tvFragmentName.setText("Cash In");
        btnSave = view.findViewById(R.id.btnSave);
        etAccountTitle = view.findViewById(R.id.etAccountTitle);
        etAmount = view.findViewById(R.id.etAmount);
        tvAmount = view.findViewById(R.id.tvAmount);
        tvTotal = view.findViewById(R.id.tvTotal);
        tvVatType = view.findViewById(R.id.tvVatType);
        tvTax = view.findViewById(R.id.tvTax);
        switchVAT = view.findViewById(R.id.switchVAT);
        linearLayoutVAT = view.findViewById(R.id.linearLayoutVAT);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveToDB();
            }
        });

        CashIn cashIn = realm.where(CashIn.class).equalTo("uuid", selectedCashIn).findFirst();
        if (cashIn != null){
            setRecords(cashIn);
        }

        if (switchVAT.isChecked()){
            linearLayoutVAT.setVisibility(View.VISIBLE);
        }
        else {
            linearLayoutVAT.setVisibility(View.GONE);
        }

        switchVAT.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    linearLayoutVAT.setVisibility(View.VISIBLE);
                }
                else {
                    linearLayoutVAT.setVisibility(View.GONE);
                }
            }
        });


        etAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                double amount = Converter.StringToDouble(etAmount.getText().toString());
                double tax;
                double total;
                if (session.getVatRate(context) == 0){
                    tax = (session.getVatAmount(context) / 100.0);
                    double taxamount = amount / (tax + 1);
                    taxamount = taxamount * tax;
                    double inclusiveamount = amount - taxamount;
                    tvAmount.setText(Converter.DoubleToString(inclusiveamount));
                    tvTax.setText(Converter.DoubleToString(taxamount));
                    tvTotal.setText(Converter.DoubleToString(amount));
                    tvVatType.setText("(Inclusive)");
                }
                else {
                    tax = (session.getVatAmount(context) / 100.0) * amount;
                    total = amount + tax;
                    tvAmount.setText(Converter.DoubleToString(amount));
                    tvTax.setText(Converter.DoubleToString(tax));
                    tvTotal.setText(Converter.DoubleToString(total));
                    tvVatType.setText("(Exclusive)");
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private void setRecords(CashIn cashIn){
        etAccountTitle.setText(cashIn.getAccountTitle());
        etAmount.setText(Converter.DoubleToString(cashIn.getAccountAmount()));

        tvAmount.setText(etAmount.getText().toString());
        double amount = Converter.StringToDouble(tvAmount.getText().toString());
        double tax = cashIn.getVAT() / amount;
        tax = tax * 100;

        double total = amount + cashIn.getVAT();



        if (cashIn.getHasVAT() == 1){
            switchVAT.setChecked(true);
            tvTax.setText(Converter.DoubleToString(cashIn.getVAT()));
            tvTotal.setText(Converter.DoubleToString(total));
            if (session.getVatRate(context) == 1){
                tvVatType.setText("(Exclusive)");
            }
            else {
                tvVatType.setText("(Inclusive)");
            }

        }
        else {
            if (session.getVatRate(context) == 1){
                tvVatType.setText("(Exclusive)");
                tax = (session.getVatAmount(context) / 100.0) * amount;
                total = amount + tax;
                tvTax.setText(Converter.DoubleToString(tax));
                tvTotal.setText(Converter.DoubleToString(total));
            }
            else {
                tvVatType.setText("(Inclusive)");
                tax = (session.getVatAmount(context) / 100.0);
                double taxamount = amount / (tax + 1);
                taxamount = taxamount * tax;
                double inclusiveamount = amount - taxamount;
                tvTax.setText(Converter.DoubleToString(taxamount));
                tvTotal.setText(Converter.DoubleToString(inclusiveamount));
            }
        }
    }


    public void errorMessage()
    {
        if (etAmount.getText().toString().matches("") && etAccountTitle.getText().toString().matches("")){
            etAccountTitle.setError("Please enter a valid amount");
            etAccountTitle.requestFocus();
        }else if (etAccountTitle.getText().toString().matches("")){
            etAccountTitle.setError("Please enter a valid data");
            etAccountTitle.requestFocus();
        }else if (etAmount.getText().toString().matches("")){
            etAmount.setError("Please enter a valid amount");
            etAmount.requestFocus();
        } else{
            dismiss();
        }
    }

    private void saveToDB() {
        try {
            CashIn cashIn = realm.where(CashIn.class).contains("uuid", selectedCashIn).findFirst();
            realm.beginTransaction();
            if (cashIn == null) {
                CashIn newCashIn = realm.createObject(CashIn.class, UUID.randomUUID().toString());
                setFields(newCashIn);
            }
            else {
                setFields(cashIn);
            }

            realm.commitTransaction();
            dismiss();
            Toasty.success(context, "Successfully saved.").show();
        } finally {
            realm.close();
        }

    }

    private void setFields(CashIn cashIn) {
        setData();
        cashIn.setAccountTitle(accountTitle);
        cashIn.setDateModified(dateModified);
        cashIn.setBatchID(batchID);
        cashIn.setUserID(UserSession.getUserId(context));

        if (switchVAT.isChecked()){
            cashIn.setHasVAT(1);
            cashIn.setAccountAmount(Converter.StringToDouble(tvAmount.getText().toString()));
            cashIn.setVAT(Converter.StringToDouble(tvTax.getText().toString()));
        }
        else {
            cashIn.setHasVAT(0);
            cashIn.setAccountAmount(accountAmount);
            cashIn.setVAT(0.0);
        }

    }

    private void setData() {
        //Date Today
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        calendar.add(Calendar.DATE, 30);

        accountTitle = etAccountTitle.getText().toString();
        accountAmount = Converter.StringToDouble(etAmount.getText().toString());
        batchID = ((CreateBatchActivity)getActivity()).getCurrentBatchID();

        dateModified = date;
    }

}


