package com.example.xtian.cash_flow_management.DialogFragment;

import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.xtian.cash_flow_management.Activities.CreateBatchActivity;
import com.example.xtian.cash_flow_management.Fragments.CashOutFragment;
import com.example.xtian.cash_flow_management.Fragments.OpeningBalanceFragment;
import com.example.xtian.cash_flow_management.Interface.IDialogCloseInterface;
import com.example.xtian.cash_flow_management.Models.OpeningBalance;
import com.example.xtian.cash_flow_management.R;
import com.example.xtian.cash_flow_management.Utils.Converter;
import com.example.xtian.cash_flow_management.Utils.DateTimeHandler;
import com.example.xtian.cash_flow_management.Utils.Debugger;
import com.example.xtian.cash_flow_management.Utils.UserSession;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import es.dmoral.toasty.Toasty;
import io.realm.Realm;

public class OpeningBalanceDialogFragment extends DialogFragment implements IDialogCloseInterface.onDismissDialogListener {
    private Context context;
    private View view;
    private Realm realm;
    private Button btnSave;
    private EditText etAccountTitle, etAmount;
    private OpeningBalanceFragment openingBalanceFragment;
    private LinearLayout linearLayoutSwitch, linearLayoutVAT;
    private String batchID;
    private Double amount;
    private Date dateModified;
    private String message;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.dialog_fragmen_add_amount, container, false);
        context = view.getContext();
        realm = Realm.getDefaultInstance();

        if (getArguments() != null)
        {
            initializeUI();
        }

        return view;
    }

    private void initializeUI() {
        TextView tvFragmentName = view.findViewById(R.id.tvFragmentName);
        tvFragmentName.setText("Opening Balance");
        btnSave = view.findViewById(R.id.btnSave);
        etAccountTitle = view.findViewById(R.id.etAccountTitle);
        etAccountTitle.setVisibility(View.GONE);
        etAmount = view.findViewById(R.id.etAmount);
        batchID = ((CreateBatchActivity)getActivity()).getCurrentBatchID();
        linearLayoutSwitch  = view.findViewById(R.id.linearLayoutSwitch);
        linearLayoutVAT = view.findViewById(R.id.linearLayoutVAT);
        linearLayoutSwitch.setVisibility(View.GONE);
        linearLayoutVAT.setVisibility(View.GONE);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveToDB();
            }
        });
    }

    private void saveToDB() {
        OpeningBalance openingBalance =  realm.where(OpeningBalance.class).equalTo
                ("batchID", batchID).findFirst();

        if (openingBalance != null) {
            openingBalance = realm.where(OpeningBalance.class).equalTo("uuid", openingBalance.getUuid()).findFirst();
        }

        realm.beginTransaction();
        if (openingBalance == null) {
            OpeningBalance newOpeningBalance = realm.createObject(OpeningBalance.class, UUID.randomUUID().toString());
            message = "saved";
            setFields(newOpeningBalance);

        }

        else {
            message = "updated";
            setFields(openingBalance);
        }
        realm.commitTransaction();
        dismiss();
        Toasty.success(context, "Data successfully " + message).show();

    }


    private void setFields(OpeningBalance openingBalance) {
        setData();
        openingBalance.setAmount(amount);
        openingBalance.setDateModified(dateModified);
        openingBalance.setBatchID(batchID);
        openingBalance.setUserID(UserSession.getUserId(context));
    }

    public void errorMessage()
    {
        if (etAmount.getText().toString().matches("")){
            etAmount.setError("Please enter a valid amount");
            etAmount.requestFocus();
        } else{
            dismiss();
        }
    }{

    }

    private void setData() {
        //Date Today
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        calendar.add(Calendar.DATE, 30);

        batchID = ((CreateBatchActivity)getActivity()).getCurrentBatchID();
        amount = Converter.StringToDouble(etAmount.getText().toString());
        dateModified = date;
    }

    @Override
    public void onDissmissDialog() {

    }
}
