package com.example.xtian.cash_flow_management.Adapters;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.example.xtian.cash_flow_management.Activities.CreateBatchActivity;
import com.example.xtian.cash_flow_management.Activities.ViewBatchActivity;
import com.example.xtian.cash_flow_management.DialogFragment.CurrencyDialogFragment;
import com.example.xtian.cash_flow_management.DialogFragment.DeleteDialogFragment;
import com.example.xtian.cash_flow_management.DialogFragment.OpeningBalanceDialogFragment;
import com.example.xtian.cash_flow_management.Models.Batch;
import com.example.xtian.cash_flow_management.Models.CashIn;
import com.example.xtian.cash_flow_management.Models.CashOut;
import com.example.xtian.cash_flow_management.Models.OpeningBalance;
import com.example.xtian.cash_flow_management.R;
import com.example.xtian.cash_flow_management.Utils.Converter;
import com.example.xtian.cash_flow_management.Utils.DateTimeHandler;
import com.example.xtian.cash_flow_management.Utils.Debugger;
import com.example.xtian.cash_flow_management.Utils.UserSession;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import io.realm.Realm;
import io.realm.RealmResults;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;

public class BatchAdapter extends ArrayAdapter<Batch> implements ListAdapter {

    private final int mResource;
    private int position;
    private Context mContext;
    private List<Batch> readingList = new ArrayList<>();
    private Realm realm;
    private UserSession session;
    private Activity thisActivity;
    private android.support.v4.app.FragmentManager fragmentManager;

    public BatchAdapter(@NonNull Context context, RealmResults<Batch> list, Activity activity, android.support.v4.app.FragmentManager fm) {
        super(context, 0 , list);
        mContext = context;
        mResource = 0;
        thisActivity = activity;
        readingList = list;
        fragmentManager = fm;
    }

    @Override
    public int getPosition(@Nullable Batch item)
    {
        int x=0;
        return super.getPosition(item);
    }
    public void setPosition(int _position)
    {
        position = _position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return createViewFromResource(LayoutInflater.from(getContext()), position, convertView, parent, mResource);
    }

    private @NonNull
    View createViewFromResource(@NonNull LayoutInflater inflater, int position,
                                @Nullable View convertView, @NonNull ViewGroup parent, int resource)
    {
        int modulus = position % 2;
        final TextView date;
        final TextView timeFrame;
        final ImageButton more;
        final CardView batchLayout;
        realm = Realm.getDefaultInstance();
        final Batch batch = readingList.get(position);

        if(convertView == null) {
            convertView= LayoutInflater.from(mContext).inflate(R.layout.list_row_batches, parent, false);
        }
//        if (modulus == 0){
//            background = convertView.findViewById(R.id.lvBatch);
//            background.setBackgroundColor(Color.parseColor("#FFF6F6F6"));
//        }
//        else {
//            background = convertView.findViewById(R.id.lvBatch);
//            background.setBackgroundColor(Color.parseColor("#FFBAF9BF"));
//        }


        date =  convertView.findViewById(R.id.tvDate);
        timeFrame =  convertView.findViewById(R.id.tvTimeFrame);
        more = convertView.findViewById(R.id.imageButton);
//        loadInstruction7(more);
        batchLayout = convertView.findViewById(R.id.linearLayoutBatch);
        batchLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PopupMenu popupMenu = new PopupMenu(mContext, more);
                popupMenu.getMenuInflater().inflate(R.menu.selected_item_menu, popupMenu.getMenu());
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.edit:
                                Intent intent = new Intent(mContext, CreateBatchActivity.class);
                                intent.putExtra("BATCH", batch);
                                mContext.startActivity(intent);
                                return true;
                            case R.id.remove:
                                loadDeleteDialogFragment(batch);

                                return true;
                            case R.id.view:
                                Intent view_intent = new Intent(mContext, ViewBatchActivity.class);
                                view_intent.putExtra("VIEWBATCH", batch);
                                mContext.startActivity(view_intent);
                                return true;
                            default:
                                return true;
                        }
                    }
                });
                popupMenu.show();
            }
        });
        date.setText(DateTimeHandler.convertDatetoMonthWord(batch.getDateModified()));
        timeFrame.setText(batch.getTimeFrame());
        return convertView;
    }

//    private void loadInstruction7(ImageButton btnmore){
//        if (session.getUserGuide(mContext) == 6){
//            new MaterialShowcaseView.Builder(thisActivity).setTarget(btnmore)
//                    .setDismissText("Got it")
//                    .setContentText("Step 7 - You can edit and remove your transactions as well")
//                    .setDelay(500)
//                    .show();
//            setUserGuide(7);
//        }
//    }

    private void loadDeleteDialogFragment(Batch batch){
        String str = "DELETE";
        Bundle bundle = new Bundle();
        bundle.putString("BATCHID", batch.getUuid());
        bundle.putString("TAG", str);
        DeleteDialogFragment deleteDialogFragment = new DeleteDialogFragment();
        deleteDialogFragment.setArguments(bundle);
        deleteDialogFragment.show(fragmentManager, "DELETE");
    }

    private void setUserGuide(int guide){
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(mContext);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt("USER_GUIDE", guide);
        editor.apply();
    }



}
