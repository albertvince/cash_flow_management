package com.example.xtian.cash_flow_management.Models;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.xtian.cash_flow_management.Utils.DateTimeHandler;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

public class CashOut extends RealmObject implements Parcelable {

    @PrimaryKey
    @Required
    private String uuid;
    private String accountTitle;
    private Double accountAmount;
    private Date dateModified;
    private String batchID;
    private Integer hasVAT;
    private Double VAT;

    @Index
    private int userID;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
    public String getAccountTitle() {
        return accountTitle;
    }

    public void setAccountTitle(String accountTitle) {
        this.accountTitle = accountTitle;
    }

    public Date getDateModified() {
        return dateModified;
    }

    public void setDateModified(Date dateModified) {
        this.dateModified = dateModified;
    }

    public Double getAccountAmount() {
        return accountAmount;
    }

    public void setAccountAmount(Double accountAmount) {
        this.accountAmount = accountAmount;
    }

    public String getBatchID() {
        return batchID;
    }

    public void setBatchID(String batchID) {
        this.batchID = batchID;
    }

    public Integer getHasVAT() {
        return hasVAT;
    }

    public void setHasVAT(Integer hasVAT) {
        this.hasVAT = hasVAT;
    }

    public Double getVAT() {
        return VAT;
    }

    public void setVAT(Double VAT) {
        this.VAT = VAT;
    }


    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public CashOut() {

    }



    protected CashOut(Parcel in) {
        setUuid(in.readString());
        setAccountTitle(in.readString());
        setAccountAmount(in.readDouble());
        setDateModified(DateTimeHandler.convertStringtoDate(in.readString()));
        setBatchID(in.readString());
        setHasVAT(in.readInt());
        setVAT(in.readDouble());
    }

    public static final Creator<CashOut> CREATOR = new Creator<CashOut>() {
        @Override
        public CashOut createFromParcel(Parcel in) {
            return new CashOut(in);
        }

        @Override
        public CashOut[] newArray(int size) {
            return new CashOut[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(getUuid());
        parcel.writeString(getAccountTitle());
        parcel.writeDouble(getAccountAmount());
        parcel.writeString(DateTimeHandler.convertDatetoStringDate(getDateModified()));
        parcel.writeString(getBatchID());
        parcel.writeInt(getHasVAT());
        parcel.writeDouble(getVAT());
    }


}
