package com.example.xtian.cash_flow_management.Models;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.xtian.cash_flow_management.Utils.DateTimeHandler;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

public class OpeningBalance extends RealmObject implements Parcelable {

    @PrimaryKey
    @Required
    private String uuid;
    private Double amount;
    private Date dateModified;
    private String batchID;

    @Index
    private int userID;

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Date getDateModified() {
        return dateModified;
    }

    public void setDateModified(Date dateModified) {
        this.dateModified = dateModified;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getBatchID() {
        return batchID;
    }

    public void setBatchID(String batchID) {
        this.batchID = batchID;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public OpeningBalance() {

    }

    protected OpeningBalance(Parcel in) {
        setUuid(in.readString());
        setAmount(in.readDouble());
        setDateModified(DateTimeHandler.convertStringtoDate(in.readString()));
        setBatchID(in.readString());
    }

    public static final Creator<OpeningBalance> CREATOR = new Creator<OpeningBalance>() {
        @Override
        public OpeningBalance createFromParcel(Parcel in) {
            return new OpeningBalance(in);
        }

        @Override
        public OpeningBalance[] newArray(int size) {
            return new OpeningBalance[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(getUuid());
        parcel.writeDouble(getAmount());
        parcel.writeString(DateTimeHandler.convertDatetoStringDate(getDateModified()));
        parcel.writeString(getBatchID());
    }
}
