package com.example.xtian.cash_flow_management.Utils;

public class Converter {

    public static double StringToDouble(String value)
    {
//        Debugger.logD(value);
        if (value == null || value.length() <= 0)
        {
            return 0.00;
        }

        return Double.parseDouble(value);
    }

    public static String DoubleToString(Double value)
    {
        return String.valueOf(value);
    }


}
