package com.example.xtian.cash_flow_management;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.example.xtian.cash_flow_management.Activities.LoginActivity;
import com.example.xtian.cash_flow_management.Activities.WelcomeActivity;
import com.example.xtian.cash_flow_management.DialogFragment.CurrencyDialogFragment;
import com.example.xtian.cash_flow_management.DialogFragment.TimeFrameDialogFragment;
import com.example.xtian.cash_flow_management.Fragments.BatchFragment;
import com.example.xtian.cash_flow_management.Fragments.SettingsFragment;
import com.example.xtian.cash_flow_management.Utils.Debugger;
import com.example.xtian.cash_flow_management.Utils.UserSession;


import es.dmoral.toasty.Toasty;
import io.realm.Realm;
import io.realm.RealmConfiguration;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public FloatingActionButton fab;
    private int selectedNavId;
    private Context context;
    private FragmentManager fragmentManager;
    private View navHeadView;
    private DrawerLayout drawer;
    private long backPressedTime;
    private TextView tvEmail;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Realm.init(this);

        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                .name("cashflowmanagement.realm")
                .schemaVersion(1)
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fragmentManager = getSupportFragmentManager();
        context = this;

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });


        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close){
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                switchFragment();
            }
        };

        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        navHeadView = navigationView.getHeaderView(0);
        tvEmail = navHeadView.findViewById(R.id.tvEmail);
        navigationView.setCheckedItem(R.id.nav_batches);

        checkUserSession();
    }

    private void setDefault(){
        UserSession session = new UserSession();
        if (session.getTimeFrame(context).equals("")){
            String defaultTimeFrame = "Weekly";
            String defaultCanvasType = "Review";
            SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
            SharedPreferences.Editor editor = settings.edit();
            editor.putString("TIME_FRAME", defaultTimeFrame);
            editor.putString("CANVAS_TYPE", defaultCanvasType);
            editor.putInt("USER_GUIDE", 0);
            editor.commit();
        }
    }

    private void checkUserSession()
    {
        if (UserSession.getToken(getApplicationContext()) == null) {
            Intent login_intent = new Intent(this, WelcomeActivity.class);
            startActivity(login_intent);
            this.finish();
        } else {
            tvEmail.setText(UserSession.getCompanyEmail(context));
            setDefault();
            openBatchFragment();
        }
    }




    @Override
    public void onBackPressed() {
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        if (drawer.isDrawerOpen(GravityCompat.START)) {
//            drawer.closeDrawer(GravityCompat.START);
//        } else {
//            super.onBackPressed();
//        }

        if (backPressedTime + 1500 > System.currentTimeMillis()){
            super.onBackPressed();
            return;
        }
        else{
            Toasty.info(context, "Press BACK again to exit").show();
        }
        backPressedTime = System.currentTimeMillis();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            UserSession.clearSession(context);
//            checkUserSession();
            Intent login_intent = new Intent(this, LoginActivity.class);
            startActivity(login_intent);
            this.finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private void switchFragment()
    {
        switch (selectedNavId)
        {
            case R.id.nav_batches : {
                openBatchFragment();
                fab.show();
                hideKeyboard();
                break;
            }

            case R.id.nav_settings : {
                openSettingsFragment();
                fab.hide();
                hideKeyboard();
                break;
            }
        }
    }

    private void openBatchFragment()
    {
        replaceFragment(BatchFragment.class, null);
    }

    private void openSettingsFragment() {replaceFragment(SettingsFragment.class, null);}

    public void replaceFragment(Class fragmentClass, Bundle bundle)
    {
        Fragment fragment = null;
        try {
            fragment = (Fragment) fragmentClass.newInstance();
            fragment.setArguments(bundle);
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Insert the fragment by replacing any existing fragment
        fragmentManager.beginTransaction().replace(R.id.content_main, fragment).commit();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        selectedNavId = item.getItemId();
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void hideKeyboard()
    {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public FloatingActionButton getFloatingActionButton(){
        return fab;
    }

}
