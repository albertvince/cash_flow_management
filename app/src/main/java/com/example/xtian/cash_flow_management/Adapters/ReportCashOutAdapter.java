package com.example.xtian.cash_flow_management.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.example.xtian.cash_flow_management.Models.CashIn;
import com.example.xtian.cash_flow_management.Models.CashOut;
import com.example.xtian.cash_flow_management.R;
import com.example.xtian.cash_flow_management.Utils.DateTimeHandler;
import com.example.xtian.cash_flow_management.Utils.UserSession;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import io.realm.RealmResults;

public class ReportCashOutAdapter extends ArrayAdapter<CashOut> implements ListAdapter {

    private final int mResource;
    private int position;
    private Context mContext;
    private List<CashOut> readingList = new ArrayList<>();
    private UserSession session;

    public ReportCashOutAdapter(@NonNull Context context, RealmResults<CashOut> list) {
        super(context, 0 , list);
        mContext = context;
        mResource = 0;
        readingList = list;
    }

    @Override
    public int getPosition(@Nullable CashOut item) {
        int x=0;
        return super.getPosition(item);
    }
    public void setPosition(int _position)
    {
        position = _position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return createViewFromResource(LayoutInflater.from(getContext()), position, convertView, parent, mResource);
    }

    private @NonNull
    View createViewFromResource(@NonNull LayoutInflater inflater, int position,
                                @Nullable View convertView, @NonNull ViewGroup parent, int resource)
    {
        final TextView accountTitle;
        final TextView amount;
        final TextView tax;
        final TextView date;
        final TextView total;
        DecimalFormat decimalFormat = new DecimalFormat("#,###.00");

        CashOut cashOut = readingList.get(position);

        if(convertView == null) {
            convertView= LayoutInflater.from(mContext).inflate(R.layout.list_row_report_amount, parent, false);
        }

        // Initializing report
        accountTitle =  convertView.findViewById(R.id.tvAccountTitle);
        amount =  convertView.findViewById(R.id.tvAmount);
        tax = convertView.findViewById(R.id.tvTax);
        date = convertView.findViewById(R.id.tvDate);
        total = convertView.findViewById(R.id.tvTotal);

        // Reading Cash Out report data
        accountTitle.setText(cashOut.getAccountTitle());

        if (cashOut.getAccountAmount() > 0){
            String stramount =  decimalFormat.format(cashOut.getAccountAmount());
            amount.setText(session.getCurrency(mContext) + stramount);
        }
        else {
            amount.setText(session.getCurrency(mContext) + "0.00");
        }

        if (cashOut.getVAT() > 0){
            String strtax = decimalFormat.format(cashOut.getVAT());
            tax.setText(session.getCurrency(mContext) + strtax);
        }
        else {
            tax.setText(session.getCurrency(mContext) + "0.00");
        }

        if (cashOut.getVAT() + cashOut.getAccountAmount() == 0){
            total.setText(session.getCurrency(mContext) + "0.00");
        }
        else {
            String strtotal = decimalFormat.format(cashOut.getVAT() + cashOut.getAccountAmount());
            total.setText(session.getCurrency(mContext) + strtotal);
        }

        date.setText(DateTimeHandler.convertDatetoMonthWord(cashOut.getDateModified()));

        return convertView;
    }

}
