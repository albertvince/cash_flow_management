package com.example.xtian.cash_flow_management.Fragments;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.example.xtian.cash_flow_management.Activities.CreateBatchActivity;
import com.example.xtian.cash_flow_management.Adapters.CashInAdapter;
import com.example.xtian.cash_flow_management.DialogFragment.CashInDialogFragment;
import com.example.xtian.cash_flow_management.Models.CashIn;
import com.example.xtian.cash_flow_management.R;
import com.example.xtian.cash_flow_management.Utils.Debugger;
import com.example.xtian.cash_flow_management.Utils.UserSession;

import java.text.DecimalFormat;
import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

public class CashInFragment extends Fragment{
    private View view;
    private Context context;
    private Realm realm;
    private RealmChangeListener realmChangeListener;
    private RealmResults<CashIn> cashInRealmResults;
    private View emptyIndicator;
    public String accountTitle;
    private CashInAdapter cashInAdapter;
    private CashIn selectedCashIn;
    private ListView lvRecords;
    private String batchID;
    private TextView tvTotal, tvTotalGST, tvTotalLabel, tvTotalExcludeLabel;
    private UserSession session;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_cash_in, container,false);
        context = getContext();
        realm = Realm.getDefaultInstance();
        return view;

    }

    @Override
    public void onStart() {
        super.onStart();
        initializeUI();
        registerForContextMenu(lvRecords);
    }

    public static CashInFragment newInstance() {
        CashInFragment fragment = new CashInFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @SuppressLint("RestrictedApi")
    private void initializeUI() {
        lvRecords = view.findViewById(R.id.lvCashInRecords);

        emptyIndicator = (View) view.findViewById(R.id.viewEmptyListIndicator);
        batchID = ((CreateBatchActivity)getActivity()).getCurrentBatchID();
        tvTotal = view.findViewById(R.id.tvTotal);
        tvTotalGST = view.findViewById(R.id.tvTotalGST);
        tvTotalLabel = view.findViewById(R.id.tvTotalLabel);
        tvTotalExcludeLabel = view.findViewById(R.id.tvTotalExcludeLabel);

        readRecords(batchID);
        readTotalAmount(batchID);
        readTotalGSTAmount(batchID);
        lvRecords.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedCashIn = cashInAdapter.getItem(position);
                lvRecords.showContextMenu();
            }
        });

        if (session.getTaxTerms(context) != null){
            tvTotalLabel.setText("Total " + session.getTaxTerms(context));
            tvTotalExcludeLabel.setText("Total excluding " + session.getTaxTerms(context));
        }
        else {
            tvTotalLabel.setText("Total Tax");
            tvTotalExcludeLabel.setText("Total excluding Tax");
        }

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        menu.add(Menu.NONE, 0, 0, "Edit");
        menu.add(Menu.NONE, 1, 1, "Remove");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getItemId() == 0){
            openCashInDialogFragment();
        }
        else if (item.getItemId() == 1){
            deleteRecord();
        }
        else {
            return false;
        }
        return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        realm.removeAllChangeListeners();
        realm.close();
    }

    private void openCashInDialogFragment(){
        String str = selectedCashIn.getUuid();
        Bundle bundle = new Bundle();
        bundle.putString("TAG", str);
        FragmentManager fm = getFragmentManager();
        CashInDialogFragment cashInDialogFragment = new CashInDialogFragment();
        cashInDialogFragment.setArguments(bundle);
        cashInDialogFragment.show(fm, "CASHIN");
    }

    private void readRecords(String search) {
        cashInRealmResults = realm.where(CashIn.class).equalTo
                ("batchID", search).findAll();
        cashInRealmResults.load();


        cashInAdapter = new CashInAdapter(context, cashInRealmResults);

        realmChangeListener = new RealmChangeListener() {
            @Override
            public void onChange(Object o) {
                if (cashInAdapter != null){
                    cashInAdapter.notifyDataSetChanged();
                }
                showEmptyListIndicator(cashInRealmResults.size() <= 0);
                readTotalAmount(batchID);
                readTotalGSTAmount(batchID);
            }
        };

        realm.addChangeListener(realmChangeListener);
        lvRecords.setAdapter(cashInAdapter);

        showEmptyListIndicator(cashInRealmResults.size() <= 0);
    }

    private void readTotalAmount(String search) {
        Double totalAmount = 0.0;

        RealmResults<CashIn> cashInRealmResults = realm.where(CashIn.class).equalTo
                ("batchID", search).findAll();
        ArrayList<CashIn> cashInArrayList = new ArrayList<>(cashInRealmResults);

        for (CashIn cashIn : cashInArrayList) {
            CashIn total = realm.where(CashIn.class).equalTo("accountAmount", cashIn.getAccountAmount()).findFirst();
            totalAmount += total.getAccountAmount();
        }
        DecimalFormat decimalFormat = new DecimalFormat("#,###.00");
        String numberasString = decimalFormat.format(totalAmount);
        if (totalAmount == 0){
            tvTotal.setText(session.getCurrency(context) + "0.00");
        }
        else {
            tvTotal.setText(session.getCurrency(context) + numberasString);
        }
    }

    private void readTotalGSTAmount(String search){
        Double totalGSTAmount = 0.0;

        RealmResults<CashIn> cashInRealmResults = realm.where(CashIn.class).equalTo ("batchID", search)
                .and().equalTo("hasVAT", 1).findAll();
        ArrayList<CashIn> cashInArrayList = new ArrayList<>(cashInRealmResults);

        for (CashIn cashIn : cashInArrayList) {
            CashIn totalGST = realm.where(CashIn.class).equalTo("VAT", cashIn.getVAT()).findFirst();
            totalGSTAmount += totalGST.getVAT();
        }
        DecimalFormat decimalFormat = new DecimalFormat("#,###.00");
        String numberasString = decimalFormat.format(totalGSTAmount);
        if (totalGSTAmount == 0){
            tvTotalGST.setText(session.getCurrency(context) + "0.00");
        }
        else {
            tvTotalGST.setText(session.getCurrency(context) + numberasString);
        }
    }

    private void showEmptyListIndicator(boolean show)
    {
        emptyIndicator.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    private void deleteRecord() {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<CashIn> result = realm.where(CashIn.class).equalTo("uuid", selectedCashIn.getUuid()).findAll();
                result.deleteAllFromRealm();
                readRecords(batchID);
                readTotalAmount(batchID);
                readTotalGSTAmount(batchID);
            }
        });
    }

}
