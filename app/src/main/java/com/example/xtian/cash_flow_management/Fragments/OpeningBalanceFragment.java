package com.example.xtian.cash_flow_management.Fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.xtian.cash_flow_management.Activities.CreateBatchActivity;
import com.example.xtian.cash_flow_management.Models.OpeningBalance;
import com.example.xtian.cash_flow_management.R;
import com.example.xtian.cash_flow_management.Utils.DateTimeHandler;
import com.example.xtian.cash_flow_management.Utils.Debugger;
import com.example.xtian.cash_flow_management.Utils.UserSession;

import java.text.DecimalFormat;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

public class OpeningBalanceFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private View view;
    private Context context;
    private Realm realm;
    private RealmChangeListener realmChangeListener;
    private RealmResults<OpeningBalance> openingBalanceRealmResults;
    private String batchID;
    private TextView tvAmount, tvDate;
    private UserSession session;

    public OpeningBalanceFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_opening_balance, container,false);
        context = getContext();
        realm = Realm.getDefaultInstance();
        return view;

    }
    @SuppressLint("RestrictedApi")
    @Override
    public void onStart() {
        super.onStart();
        initializeUI();

    }

    public static OpeningBalanceFragment newInstance() {
        OpeningBalanceFragment fragment = new OpeningBalanceFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }



    @SuppressLint("RestrictedApi")
    private void initializeUI() {
        batchID = ((CreateBatchActivity)getActivity()).getCurrentBatchID();
        tvAmount = view.findViewById(R.id.tvAmount);
        tvDate = view.findViewById(R.id.tvDate);
        readRecords(batchID);
        readLiveRecords(batchID);
    }

    @Override
    public void onResume() {
        super.onResume();
        readLiveRecords(batchID);
    }


    public void readRecords(String search) {
        OpeningBalance openingBalance =  realm.where(OpeningBalance.class).equalTo
                ("batchID", search).findFirst();
        if (openingBalance != null) {
            DecimalFormat decimalFormat = new DecimalFormat("#,###.00");
            String numberasString = decimalFormat.format(openingBalance.getAmount());
            tvAmount.setText(session.getCurrency(context) + numberasString);
            tvDate.setVisibility(View.VISIBLE);
            tvDate.setText("As of " + DateTimeHandler.convertDatetoMonthWord(openingBalance.getDateModified()));
        }
        else {
            tvAmount.setText(session.getCurrency(context) + "0.00");
            tvDate.setVisibility(View.GONE);
        }
    }

    private void readLiveRecords(String search){
        RealmResults<OpeningBalance> openingBalanceRealmResults = realm.where(OpeningBalance.class).equalTo
                ("batchID", search).findAll();
        openingBalanceRealmResults.load();

        openingBalanceRealmResults.addChangeListener(new RealmChangeListener<RealmResults<OpeningBalance>>() {
            @Override
            public void onChange(RealmResults<OpeningBalance> cashIns) {
                readRecords(batchID);
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onRefresh() {
        readRecords(batchID);
    }
}
