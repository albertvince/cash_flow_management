package com.example.xtian.cash_flow_management.Fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.xtian.cash_flow_management.Activities.CreateBatchActivity;
import com.example.xtian.cash_flow_management.Adapters.AvailabletoSpendAdapter;
import com.example.xtian.cash_flow_management.DialogFragment.AvailabletoSpendDialogFragment;
import com.example.xtian.cash_flow_management.Models.AvailabletoSpend;
import com.example.xtian.cash_flow_management.R;

import es.dmoral.toasty.Toasty;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

public class AvailabletoSpendFragment extends Fragment {
    private View view;
    private Context context;
    private Realm realm;
    private RealmResults<AvailabletoSpend> availabletoSpendRealmResults;
    private RealmChangeListener realmChangeListener;
    private View emptyIndicator;
    public String accountTitle;
    private AvailabletoSpendAdapter availabletoSpendAdapter;
    private ListView lvRecords;
    private String batchID;
    private TextView tvTotal;
    private AvailabletoSpend selectedAvailabletoSpend;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_available_to_spend, container,false);
        context = getContext();
        realm = Realm.getDefaultInstance();
        return view;

    }

    public void onStart() {
        super.onStart();
        initializeUI();
        registerForContextMenu(lvRecords);
    }

    @SuppressLint("RestrictedApi")
    private void initializeUI() {
        lvRecords = view.findViewById(R.id.lvAvailabletoSpendRecords);
        emptyIndicator = (View) view.findViewById(R.id.viewEmptyListIndicator);
        batchID = ((CreateBatchActivity)getActivity()).getCurrentBatchID();
        tvTotal = view.findViewById(R.id.tvTotal);
        readRecords(batchID);

        lvRecords.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedAvailabletoSpend = availabletoSpendAdapter.getItem(position);
                lvRecords.showContextMenu();
            }
        });

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        menu.add(Menu.NONE, 0, 0, "Edit");
        menu.add(Menu.NONE, 1, 1, "Remove");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getItemId() == 0){
            openAvailabletoSpendDialog();
        }
        else if (item.getItemId() == 1){
            deleteRecord();
        }
        else {
            return false;
        }
        return true;
    }

    private void readRecords(String search) {
        availabletoSpendRealmResults = realm.where(AvailabletoSpend.class).equalTo
                ("batchID", search).findAll();
        availabletoSpendRealmResults.load();
        availabletoSpendAdapter = new AvailabletoSpendAdapter(context, availabletoSpendRealmResults);

        realmChangeListener = new RealmChangeListener() {
            @Override
            public void onChange(Object o) {
                if (availabletoSpendAdapter != null){
                    availabletoSpendAdapter.notifyDataSetChanged();
                    Toasty.warning(context, "Something happened.");
                }
            }
        };

        lvRecords.setAdapter(availabletoSpendAdapter);
        showEmptyListIndicator(availabletoSpendRealmResults.size() <= 0);

    }

    private void openAvailabletoSpendDialog(){
        String str = selectedAvailabletoSpend.getUuid();
        Bundle bundle = new Bundle();
        bundle.putString("TAG", str);
        FragmentManager fm = getFragmentManager();
        AvailabletoSpendDialogFragment availabletoSpendDialogFragment = new AvailabletoSpendDialogFragment();
        availabletoSpendDialogFragment.setArguments(bundle);
        availabletoSpendDialogFragment.show(fm, "AVAILABLE TO SPEND");
    }

    public static AvailabletoSpendFragment newInstance() {
        AvailabletoSpendFragment fragment = new AvailabletoSpendFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    private void showEmptyListIndicator(boolean show)
    {
        emptyIndicator.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    private void deleteRecord() {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<AvailabletoSpend> result = realm.where(AvailabletoSpend.class).equalTo("uuid", selectedAvailabletoSpend.getUuid()).findAll();
                result.deleteAllFromRealm();
//                readRecords(batchID);
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        realm.removeAllChangeListeners();
        realm.close();
    }
}
