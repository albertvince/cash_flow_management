package com.example.xtian.cash_flow_management.Activities;

import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.example.xtian.cash_flow_management.Adapters.CashInAdapter;
import com.example.xtian.cash_flow_management.Adapters.ViewPagerAdapter;
import com.example.xtian.cash_flow_management.DialogFragment.AvailabletoSpendDialogFragment;
import com.example.xtian.cash_flow_management.DialogFragment.CashInDialogFragment;
import com.example.xtian.cash_flow_management.DialogFragment.CashOutDialogFragment;
import com.example.xtian.cash_flow_management.DialogFragment.OpeningBalanceDialogFragment;
import com.example.xtian.cash_flow_management.Fragments.AvailabletoSpendFragment;
import com.example.xtian.cash_flow_management.Fragments.CashInFragment;
import com.example.xtian.cash_flow_management.Fragments.CashOutFragment;
import com.example.xtian.cash_flow_management.Fragments.CashSurplusFragment;
import com.example.xtian.cash_flow_management.Fragments.OpeningBalanceFragment;
import com.example.xtian.cash_flow_management.Models.Batch;
import com.example.xtian.cash_flow_management.Models.CashOut;
import com.example.xtian.cash_flow_management.Models.CashSurplus;
import com.example.xtian.cash_flow_management.Models.OpeningBalance;
import com.example.xtian.cash_flow_management.R;
import com.example.xtian.cash_flow_management.Utils.DateTimeHandler;
import com.example.xtian.cash_flow_management.Utils.Debugger;
import com.example.xtian.cash_flow_management.Utils.UserSession;

import es.dmoral.toasty.Toasty;
import io.realm.Realm;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;

public class CreateBatchActivity extends AppCompatActivity {

    private Toolbar toolbar;
    public FloatingActionButton fab;
    private Batch batch;
    public String batchID;
    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    private ViewPagerAdapter mViewPagerAdapter;
    private UserSession session;
    private Context context;
    private Realm realm;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getApplicationContext();
        realm = Realm.getDefaultInstance();
        setContentView(R.layout.activity_create_batch);

    }

    private void initializeUI() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimary)));
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String str = "OPENINGBALANCE";
                Bundle bundle = new Bundle();
                bundle.putString("TAG", str);
                FragmentManager fm = getFragmentManager();
                OpeningBalanceDialogFragment openingBalanceDialogFragment = new OpeningBalanceDialogFragment();
                openingBalanceDialogFragment.setArguments(bundle);
                openingBalanceDialogFragment.show(fm, "OPENING BALANCE");
            }
        });
        getSupportActionBar().setElevation(0);

        mViewPager = findViewById(R.id.viewpager);
        mViewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        mViewPagerAdapter.addFragment(OpeningBalanceFragment.newInstance(), "Opening Balance");
        mViewPagerAdapter.addFragment(CashInFragment.newInstance(), "Cash In");
        mViewPagerAdapter.addFragment(CashOutFragment.newInstance(), "Cash Out");
        mViewPagerAdapter.addFragment(CashSurplusFragment.newInstance(), "Cash Surplus");
        mViewPagerAdapter.addFragment(AvailabletoSpendFragment.newInstance(), "Available to Spend");
        mViewPager.setAdapter(mViewPagerAdapter);
        mTabLayout = findViewById(R.id.tabs);
        mTabLayout.setupWithViewPager(mViewPager);
        loadInstruction2();

        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int i = tab.getPosition();
                switch(i) {
                    case 0 : {
                        loadInstruction2();
                        fab.show();
                        fab.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimary)));
                        fab.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String str = "OPENINGBALANCE";
                                Bundle bundle = new Bundle();
                                bundle.putString("TAG", str);
                                FragmentManager fm = getFragmentManager();
                                OpeningBalanceDialogFragment openingBalanceDialogFragment = new OpeningBalanceDialogFragment();
                                openingBalanceDialogFragment.setArguments(bundle);
                                openingBalanceDialogFragment.show(fm, "OPENING BALANCE");

                            }
                        });
                        break;
                    }
                    case 1 : {
                        loadInstruction3();
                        fab.show();
                        fab.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#FF669900")));
                        fab.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String str = "CASHIN";
                                Bundle bundle = new Bundle();
                                bundle.putString("TAG", str);
                                FragmentManager fm = getFragmentManager();
                                CashInDialogFragment cashInDialogFragment = new CashInDialogFragment();
                                cashInDialogFragment.setArguments(bundle);
                                cashInDialogFragment.show(getSupportFragmentManager(), "CASH IN");
                            }
                        });
                        break;
                    }
                    case 2 : {
                        loadInstruction4();
                        fab.show();
                        fab.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#CB0001")));
                        fab.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String str = "CASHOUT";
                                Bundle bundle = new Bundle();
                                bundle.putString("TAG", str);
                                FragmentManager fm = getFragmentManager();
                                CashOutDialogFragment cashOutDialogFragment = new CashOutDialogFragment();
                                cashOutDialogFragment.setArguments(bundle);
                                cashOutDialogFragment.show(getSupportFragmentManager(), "CASH OUT");
                            }
                        });
                        break;
                    }
                    case 3 : {
//                        loadInstruction5();
                        fab.hide();
                        break;
                    }
                    case 4 : {
//                        loadInstruction6();
                        fab.hide();
                        fab.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorGoldenYellow)));
                        fab.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String str = "AVAILABLETOSPEND";
                                Bundle bundle = new Bundle();
                                bundle.putString("TAG", str);
                                FragmentManager fm = getFragmentManager();
                                AvailabletoSpendDialogFragment availabletoSpendDialogFragment = new AvailabletoSpendDialogFragment();
                                availabletoSpendDialogFragment.setArguments(bundle);
                                availabletoSpendDialogFragment.show(getSupportFragmentManager(), "AVAILABLE TO SPEND");
                            }
                        });
                        break;
                    }
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        checkUserGuide();
    }

    @Override
    protected void onStart() {
        super.onStart();
        initializeUI();
        Intent intent = getIntent();
        if (intent.getParcelableExtra("BATCH") != null) {
            batch = intent.getParcelableExtra("BATCH");
            batch.getBatchCode();
            setRecords(batch);
        }
        else {
            batch = new Batch();
        }
    }

    private void checkUserGuide(){
        int guide = session.getUserGuide(context);
        switch (guide){
            case 2 : {
                loadInstruction2();
                break;
            }
            case 3 : {
                loadInstruction3();
                break;
            }
            case 4 : {
                loadInstruction4();
                break;
            }
            case 5 : {
//                loadInstruction5();
                break;
            }
//            case 6 : {
//                loadInstruction2();
//                break;
//            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    private void setUserGuide(int guide){
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt("USER_GUIDE", guide);
        editor.apply();
    }

    private void loadInstruction2(){
        if (session.getUserGuide(context) == 1){
            new MaterialShowcaseView.Builder(this).setTarget(fab)
                    .setDismissText("Got it")
                    .setContentText("Step 2 - Start your balance")
                    .setDelay(500)
                    .show();
            setUserGuide(2);
        }

    }

    private void loadInstruction3(){
        if (session.getUserGuide(context) == 2) {
            new MaterialShowcaseView.Builder(this).setTarget(fab)
                    .setDismissText("Got it")
                    .setContentText("Step 3 - Start your cash in")
                    .setDelay(500)
                    .show();
            setUserGuide(3);
        }

    }

    private void loadInstruction4(){
        if (session.getUserGuide(context) == 3) {
            new MaterialShowcaseView.Builder(this).setTarget(fab)
                    .setDismissText("Got it")
                    .setContentText("Step 4 - Start your cash out")
                    .setDelay(500)
                    .show();
            setUserGuide(4);
        }

    }

//    private void loadInstruction5(){
//        TextView tvAmount = findViewById(R.id.tvAmount);
//        if (session.getUserGuide(context) == 4) {
//            new MaterialShowcaseView.Builder(this)
//                    .setTarget(tvAmount)
//                    .setDismissText("Got it")
//                    .setContentText("Step 5 - This will be your surplus")
//                    .setDelay(500)
//                    .show();
//            setUserGuide(5);
//        }
//
//    }
//
//    private void loadInstruction6(){
//        ListView lvAvailabletoSpend = findViewById(R.id.lvAvailabletoSpendRecords);
//        if (session.getUserGuide(context) == 5) {
//            new MaterialShowcaseView.Builder(this).setTarget(lvAvailabletoSpend)
//                    .setDismissText("Got it")
//                    .setContentText("Step 6 - Your available to spend")
//                    .setDelay(500)
//                    .show();
//            setUserGuide(6);
//        }
//    }

    private void setRecords(Batch batch) {
        batchID = batch.getUuid();
        int batchCode = batch.getBatchCode();
        this.setTitle(batch.getTimeFrame() + " - " + DateTimeHandler.convertDatetoMonthWord(batch.getDateModified()));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.save, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_save) {
            Toasty.success(getApplicationContext(), "Successfully saved.").show();
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);

    }
    public String getCurrentBatchID() { return batchID; }

}
