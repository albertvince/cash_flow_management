package com.example.xtian.cash_flow_management.Utils;

import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateTimeHandler {

    public static String getTimeStamp(){
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd-hh-mm");
            String format = simpleDateFormat.format(new Date());

            return format.replace("-","");

        } catch (Exception err) {
            return "";
        }
    }

    public static Date getDueDate(Date date, int daysInterval){
        try {
            final Calendar c = Calendar.getInstance();
            c.setTime(date);
            c.add(Calendar.DATE, daysInterval);

            return c.getTime();

        } catch (Exception err) {
            return null;
        }
    }

    public static String convertDatetoDisplayDate(Date date){
        try {
            DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
            return dateFormat.format(date);
        } catch (Exception err) {
            return null;
        }
    }

    public static String convertDatetoMonthWord(Date date){
        try {
            DateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
            return dateFormat.format(date);
        } catch (Exception err) {
            return null;
        }
    }

    public static String convertDatetoStringDate(Date date){
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            return dateFormat.format(date);
        } catch (Exception err) {
            return null;
        }
    }
    public static String convertTimetoStringDate(Date date){
        try {
            DateFormat dateFormat = new SimpleDateFormat("hh:mm a");
            return dateFormat.format(date);
        } catch (Exception err) {
            return null;
        }
    }
    public static Date convertStringDateToTime(String strDate){
        try {
            DateFormat dateFormat = new SimpleDateFormat("hh:mm a");
            return (Date)dateFormat.parse(String.valueOf(strDate));
        } catch (Exception err) {
            return null;
        }
    }

    public static Date convertStringtoDate(String strDate){
        try {
            // used for server
            DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd");
            return originalFormat.parse(strDate);
        } catch (Exception err) {
            return null;
        }
    }

    public static Date convertStringtoDate2(String strDate){
        try {
            DateFormat originalFormat = new SimpleDateFormat("MM-dd-yyyy");
            return originalFormat.parse(strDate);
        } catch (Exception err) {
            return null;
        }
    }

    public static Date convertToTimeMonth(String strDate) {
        try {
            DateFormat originalFormat = new SimpleDateFormat("MM-dd-yyyy hh:mm");
            return originalFormat.parse(strDate);
        } catch (Exception err) {
            return null;
        }
    }
    public static Date convertToTimeToDateString(String strDate){
        try {
            DateFormat originalFormat = new SimpleDateFormat("hh:mm");
            return originalFormat.parse(strDate);
        } catch (Exception err) {
            return null;
        }
    }



    public static Date addDaysToDate(Date date, int daysCount)
    {
        final Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, daysCount);
        return c.getTime();
    }


}