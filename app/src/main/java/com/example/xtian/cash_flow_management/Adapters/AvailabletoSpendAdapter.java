package com.example.xtian.cash_flow_management.Adapters;

import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.example.xtian.cash_flow_management.Models.AvailabletoSpend;
import com.example.xtian.cash_flow_management.Models.CashIn;
import com.example.xtian.cash_flow_management.R;
import com.example.xtian.cash_flow_management.Utils.Converter;
import com.example.xtian.cash_flow_management.Utils.DateTimeHandler;
import com.example.xtian.cash_flow_management.Utils.UserSession;


import java.util.ArrayList;
import java.util.List;

import io.realm.RealmResults;

public class AvailabletoSpendAdapter extends ArrayAdapter<AvailabletoSpend> implements ListAdapter{
    private final int mResource;
    private int position;
    private Context mContext;
    private List<AvailabletoSpend> readingList = new ArrayList<>();
    private UserSession session;

    public AvailabletoSpendAdapter(@NonNull Context context, RealmResults<AvailabletoSpend> list) {
        super(context, 0 , list);
        mContext = context;
        mResource = 0;
        readingList = list;
    }

    @Override
    public int getPosition(@Nullable AvailabletoSpend item)
    {
        int x=0;
        return super.getPosition(item);
    }
    public void setPosition(int _position)
    {
        position = _position;
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return createViewFromResource(LayoutInflater.from(getContext()), position, convertView, parent, mResource);
    }

    private @NonNull
    View createViewFromResource(@NonNull LayoutInflater inflater, int position,
                                @Nullable View convertView, @NonNull ViewGroup parent, int resource)
    {
        final TextView accountTitle;
        final TextView amount;
        final TextView date;
        AvailabletoSpend availabletoSpend = readingList.get(position);

        if(convertView == null) {
            convertView= LayoutInflater.from(mContext).inflate(R.layout.list_row_cash_in, parent, false);
        }

        accountTitle =  convertView.findViewById(R.id.tvAccountTitle);
        amount =  convertView.findViewById(R.id.tvAmount);
        date = convertView.findViewById(R.id.tvDate);

        accountTitle.setText(availabletoSpend.getAccountTitle());
        amount.setText(session.getCurrency(mContext) + Converter.DoubleToString(availabletoSpend.getAccountAmount()));
        date.setText(DateTimeHandler.convertDatetoDisplayDate(availabletoSpend.getDateModified()));
        return convertView;
    }
}
