package com.example.xtian.cash_flow_management.Activities;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.example.xtian.cash_flow_management.Adapters.CashInAdapter;
import com.example.xtian.cash_flow_management.Adapters.ReportAvailabletoSpendAdapter;
import com.example.xtian.cash_flow_management.Adapters.ReportCashInAdapter;
import com.example.xtian.cash_flow_management.Adapters.ReportCashOutAdapter;
import com.example.xtian.cash_flow_management.DialogFragment.AvailabletoSpendDialogFragment;
import com.example.xtian.cash_flow_management.DialogFragment.CashOutDialogFragment;
import com.example.xtian.cash_flow_management.DialogFragment.ListRowDialogFragment;
import com.example.xtian.cash_flow_management.Models.AvailabletoSpend;
import com.example.xtian.cash_flow_management.Models.Batch;
import com.example.xtian.cash_flow_management.Models.CashIn;
import com.example.xtian.cash_flow_management.Models.CashOut;
import com.example.xtian.cash_flow_management.Models.OpeningBalance;
import com.example.xtian.cash_flow_management.R;
import com.example.xtian.cash_flow_management.Utils.DateTimeHandler;
import com.example.xtian.cash_flow_management.Utils.Tools;
import com.example.xtian.cash_flow_management.Utils.UserSession;
import com.example.xtian.cash_flow_management.Utils.ViewAnimation;
import java.text.DecimalFormat;
import java.util.ArrayList;
import io.realm.Realm;
import io.realm.RealmResults;

public class ViewBatchActivity extends AppCompatActivity {
    private Context context;
    private Realm realm;
    private TextView tvBalanceAmount, tvBalanceDate;
    private ListView lvCashin, lvCashOut, lvAvailabletoSpend;
    private TextView tvTotalCashIn, tvTotalExcludeCashIn, tvTotalCashInLabel, tvTotalExcludeCashInLabel,
            tvTotalCashOut, tvTotalExcludeCashOut, tvTotalCashOutLabel, tvTotalExcludeCashOutLabel,
            tvCashSurplusAmount, tvCashSurplusLabel, tvTotalAvailabletoSpend;
    private ReportCashInAdapter reportCashInAdapter;
    private ReportCashOutAdapter reportCashOutAdapter;
    private ReportAvailabletoSpendAdapter reportAvailabletoSpendAdapter;
    private UserSession session;
    private Batch batch;
    private String batchID;
    private Double openingbalance = 0.0;
    private Double totalcashinexclude = 0.0;
    private Double totalcashoutexclude = 0.0;
    private ImageButton btnToggleCashIn, btnToggleCashOut, btnToggleAvailabletoSpend;
    private View viewEmptyCashIn, viewEmptyCashOut, viewEmptyAvailabletoSpend;
    private View layout_expand_cashin, layout_expand_cashout, layout_expand_availabletospend;
    private NestedScrollView nested_scroll_view;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_batch);
        realm = Realm.getDefaultInstance();
        context = this;
        Intent intent = getIntent();
        if (intent.getParcelableExtra("VIEWBATCH") != null) {
            batch = intent.getParcelableExtra("VIEWBATCH");
            batch.getBatchCode();
            batchID = batch.getUuid();
            initializeUI();
            setRecords(batch);
            this.setTitle("Transaction as of " + DateTimeHandler.convertDatetoMonthWord(batch.getDateModified()));
            getSupportActionBar().setElevation(0);
        }
        else {
            batch = new Batch();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    private void initializeUI(){
        nested_scroll_view = findViewById(R.id.nested_scroll_view);


        // Initializing Opening Balance
        tvBalanceAmount = findViewById(R.id.tvBalanceAmount);
        tvBalanceDate = findViewById(R.id.tvBalanceDate);


        //Initializing Cash In
        tvTotalCashIn = findViewById(R.id.tvTotalCashIn);
        tvTotalExcludeCashIn = findViewById(R.id.tvTotalExcludeCashIn);
        tvTotalCashInLabel = findViewById(R.id.tvTotalCashInLabel);
        tvTotalExcludeCashInLabel = findViewById(R.id.tvTotalExcludeCashInLabel);
        viewEmptyCashIn = findViewById(R.id.viewEmptyCashIn);

        lvCashin = findViewById(R.id.lvCashIn);
        openCashInReports(batchID);
        lvCashin.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

        layout_expand_cashin = findViewById(R.id.layout_expand_cashin);
        layout_expand_cashin.setVisibility(View.GONE);

        btnToggleCashIn = findViewById(R.id.btnToggleCashIn);
        btnToggleCashIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleCashIn(btnToggleCashIn);
            }
        });

        // -------------------------------------------------------------------  //

        //Initializing Cash Out
        tvTotalCashOut = findViewById(R.id.tvTotalCashOut);
        tvTotalExcludeCashOut = findViewById(R.id.tvTotalExcludeCashOut);
        tvTotalCashOutLabel = findViewById(R.id.tvTotalCashOutLabel);
        tvTotalExcludeCashOutLabel = findViewById(R.id.tvTotalExcludeCashOutLabel);
        viewEmptyCashOut = findViewById(R.id.viewEmptyCashOut);

        lvCashOut = findViewById(R.id.lvCashOut);
        openCashOutReports(batchID);
        lvCashOut.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

        layout_expand_cashout = findViewById(R.id.layout_expand_cashout);
        layout_expand_cashout.setVisibility(View.GONE);

        btnToggleCashOut = findViewById(R.id.btnToggleCashOut);
        btnToggleCashOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleCashOut(btnToggleCashOut);
            }
        });

        // -------------------------------------------------------------------  //

        //Initializing Cash Surplus
        tvCashSurplusAmount = findViewById(R.id.tvCashSurplusAmount);
        tvCashSurplusLabel = findViewById(R.id.tvCashSurplusLabel);

        // -------------------------------------------------------------------  //

        //Initializing Available to Spend
        tvTotalAvailabletoSpend = findViewById(R.id.tvTotalAvailabletoSpend);
        viewEmptyAvailabletoSpend = findViewById(R.id.viewEmptyAvailabletoSpend);
        lvAvailabletoSpend = findViewById(R.id.lvAvailabletoSpend);
        openAvailabletoSpendReports(batchID);
        lvAvailabletoSpend.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

        layout_expand_availabletospend = findViewById(R.id.layout_expand_availabletospend);
        layout_expand_availabletospend.setVisibility(View.GONE);

        btnToggleAvailabletoSpend = findViewById(R.id.btnToggleAvailabletoSpend);
        btnToggleAvailabletoSpend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleAvailabletoSpend(btnToggleAvailabletoSpend);
            }
        });

        // -------------------------------------------------------------------  //

        // Setting all Records
        setRecords(batch);

        // Setting all Labels
        setLabels();
    }

    private void setRecords(Batch batch) {
        batchID = batch.getUuid();
        setOpeningBalance(batchID);
        setTotalCashIn(batchID);
        setTotalExcludeCashIn(batchID);
        setTotalCashOut(batchID);
        setTotalExcludeCashOut(batchID);
        setTotalAvailabletoSpend(batchID);
        setCashSurplus();
    }

    private void setOpeningBalance(String search){
        OpeningBalance openingBalance =  realm.where(OpeningBalance.class).equalTo
                ("batchID", search).findFirst();
        if (openingBalance != null) {
            DecimalFormat decimalFormat = new DecimalFormat("#,###.00");
            String numberasString = decimalFormat.format(openingBalance.getAmount());
            tvBalanceAmount.setText(session.getCurrency(context) + numberasString);
            tvBalanceDate.setText(DateTimeHandler.convertDatetoMonthWord(openingBalance.getDateModified()));
            openingbalance = openingBalance.getAmount();
        }
        else {
            tvBalanceAmount.setText(session.getCurrency(context) + "0.00");
            tvBalanceDate.setText("DD-MM-YYYY");
        }

    }

    private void setTotalCashIn(String search){
        Double totalGSTAmount = 0.0;

        RealmResults<CashIn> cashInRealmResults = realm.where(CashIn.class).equalTo ("batchID", search)
                .and().equalTo("hasVAT", 1).findAll();
        ArrayList<CashIn> cashInArrayList = new ArrayList<>(cashInRealmResults);

        for (CashIn cashIn : cashInArrayList) {
            CashIn totalGST = realm.where(CashIn.class).equalTo("VAT", cashIn.getVAT()).findFirst();
            totalGSTAmount += totalGST.getVAT();
        }
        DecimalFormat decimalFormat = new DecimalFormat("#,###.00");
        String numberasString = decimalFormat.format(totalGSTAmount);
        if (totalGSTAmount == 0){
            tvTotalCashIn.setText(session.getCurrency(context) + "0.00");
        }
        else {
            tvTotalCashIn.setText(session.getCurrency(context) + numberasString);
        }
    }

    private void toggleCashIn(View view) {
        boolean show = toggleArrow(view);
        if (show) {
            ViewAnimation.expand(layout_expand_cashin, new ViewAnimation.AnimListener() {
                @Override
                public void onFinish() {
                    Tools.nestedScrollTo(nested_scroll_view, layout_expand_cashin);
                }
            });
        } else {
            ViewAnimation.collapse(layout_expand_cashin);
        }
    }

    private void toggleCashOut(View view) {
        boolean show = toggleArrow(view);
        if (show) {
            ViewAnimation.expand(layout_expand_cashout, new ViewAnimation.AnimListener() {
                @Override
                public void onFinish() {
                    Tools.nestedScrollTo(nested_scroll_view, layout_expand_cashout);
                }
            });
        } else {
            ViewAnimation.collapse(layout_expand_cashout);
        }
    }

    private void toggleAvailabletoSpend(View view) {
        boolean show = toggleArrow(view);
        if (show) {
            ViewAnimation.expand(layout_expand_availabletospend, new ViewAnimation.AnimListener() {
                @Override
                public void onFinish() {
                    Tools.nestedScrollTo(nested_scroll_view, layout_expand_availabletospend);
                }
            });
        } else {
            ViewAnimation.collapse(layout_expand_availabletospend);
        }
    }

    public boolean toggleArrow(View view) {
        if (view.getRotation() == 0) {
            view.animate().setDuration(200).rotation(180);
            return true;
        } else {
            view.animate().setDuration(200).rotation(0);
            return false;
        }
    }

    private void setTotalExcludeCashIn(String search){
        Double totalAmount = 0.0;

        RealmResults<CashIn> cashInRealmResults = realm.where(CashIn.class).equalTo
                ("batchID", search).findAll();
        ArrayList<CashIn> cashInArrayList = new ArrayList<>(cashInRealmResults);

        for (CashIn cashIn : cashInArrayList) {
            CashIn total = realm.where(CashIn.class).equalTo("accountAmount", cashIn.getAccountAmount()).findFirst();
            totalAmount += total.getAccountAmount();
        }
        DecimalFormat decimalFormat = new DecimalFormat("#,###.00");
        String numberasString = decimalFormat.format(totalAmount);
        if (totalAmount == 0) {
            tvTotalExcludeCashIn.setText(session.getCurrency(context) + "0.00");
        } else {
            tvTotalExcludeCashIn.setText(session.getCurrency(context) + numberasString);
            totalcashinexclude = totalAmount;
        }

    }

    private void setTotalCashOut(String search){
        Double totalGSTAmount = 0.0;

        RealmResults<CashOut> cashOutRealmResults = realm.where(CashOut.class).equalTo ("batchID", search)
                .and().equalTo("hasVAT", 1).findAll();
        ArrayList<CashOut> cashOutArrayList = new ArrayList<>(cashOutRealmResults);

        for (CashOut cashOut : cashOutArrayList) {
            CashOut totalGST = realm.where(CashOut.class).equalTo("VAT", cashOut.getVAT()).findFirst();
            totalGSTAmount += totalGST.getVAT();
        }
        DecimalFormat decimalFormat = new DecimalFormat("#,###.00");
        String numberasString = decimalFormat.format(totalGSTAmount);
        if (totalGSTAmount == 0){
            tvTotalCashOut.setText(session.getCurrency(context) + "0.00");
        }
        else {
            tvTotalCashOut.setText(session.getCurrency(context) + numberasString);
        }
    }

    private void setTotalExcludeCashOut(String search){
        Double totalAmount = 0.0;

        RealmResults<CashOut> cashOutRealmResults = realm.where(CashOut.class).equalTo
                ("batchID", search).findAll();
        ArrayList<CashOut> cashOutArrayList = new ArrayList<>(cashOutRealmResults);

        for (CashOut cashOut : cashOutArrayList) {
            CashOut total = realm.where(CashOut.class).equalTo("accountAmount", cashOut.getAccountAmount()).findFirst();
            totalAmount += total.getAccountAmount();
        }
        DecimalFormat decimalFormat = new DecimalFormat("#,###.00");
        String numberasString = decimalFormat.format(totalAmount);
        if (totalAmount == 0){
            tvTotalExcludeCashOut.setText(session.getCurrency(context) + "0.00");
        }
        else {
            tvTotalExcludeCashOut.setText(session.getCurrency(context) + numberasString);
            totalcashoutexclude = totalAmount;
        }
    }

    private void setTotalAvailabletoSpend(String search){
        Double totalAmount = 0.0;

        RealmResults<AvailabletoSpend> availabletoSpendRealmResults = realm.where(AvailabletoSpend.class).equalTo
                ("batchID", search).findAll();
        ArrayList<AvailabletoSpend> availabletoSpendArrayList = new ArrayList<>(availabletoSpendRealmResults);

        for (AvailabletoSpend availabletoSpend : availabletoSpendArrayList) {
            AvailabletoSpend total = realm.where(AvailabletoSpend.class).equalTo("accountAmount", availabletoSpend.getAccountAmount()).findFirst();
            totalAmount += total.getAccountAmount();
        }
        DecimalFormat decimalFormat = new DecimalFormat("#,###.00");
        String numberasString = decimalFormat.format(totalAmount);
        if (totalAmount == 0){
            tvTotalAvailabletoSpend.setText(session.getCurrency(context) + "0.00");
        }
        else {
            tvTotalAvailabletoSpend.setText(session.getCurrency(context) + numberasString);
        }
    }

    private void setLabels(){
        if (session.getTaxTerms(context) != null){
            //Cash in Label
            tvTotalCashInLabel.setText("Total " + session.getTaxTerms(context));
            tvTotalExcludeCashInLabel.setText("Total excluding " + session.getTaxTerms(context));

            //Cash Out Label
            tvTotalCashOutLabel.setText("Total " + session.getTaxTerms(context));
            tvTotalExcludeCashOutLabel.setText("Total excluding " + session.getTaxTerms(context));

        }
        else {
            //Cash in Label
            tvTotalCashInLabel.setText("Total Tax");
            tvTotalExcludeCashInLabel.setText("Total excluding Tax");

            //Cash Out Label
            tvTotalCashOutLabel.setText("Total Tax");
            tvTotalExcludeCashOutLabel.setText("Total excluding Tax");
        }
    }

    private void setCashSurplus(){
        Double cashsurplus = (openingbalance + totalcashinexclude) - totalcashoutexclude;
        DecimalFormat decimalFormat = new DecimalFormat("#,###.00");
        String numberasString = decimalFormat.format(cashsurplus);

        if (cashsurplus == 0){
            tvCashSurplusAmount.setText(session.getCurrency(context) + "0.00");
        }
        else {
            tvCashSurplusAmount.setText(session.getCurrency(context) + numberasString);
        }

        if (session.getTaxTerms(context) != null){
            tvCashSurplusLabel.setText("Total excluding " + session.getTaxTerms(context));
        }
        else {
            tvCashSurplusLabel.setText("Total excluding Tax");
        }
    }

    private void openCashInReports(String search){
        RealmResults<CashIn> cashInRealmResults = realm.where(CashIn.class).equalTo
                ("batchID", search).findAll();
        cashInRealmResults.load();

        reportCashInAdapter = new ReportCashInAdapter(context, cashInRealmResults);
        lvCashin.setAdapter(reportCashInAdapter);

        reportCashInAdapter.notifyDataSetChanged();
        showEmptyListIndicator(cashInRealmResults.size() <= 0, lvCashin, viewEmptyCashIn);
    }

    private void openCashOutReports(String search){
        RealmResults<CashOut> cashOutRealmResults = realm.where(CashOut.class).equalTo
                ("batchID", search).findAll();
        cashOutRealmResults.load();

        reportCashOutAdapter = new ReportCashOutAdapter(context, cashOutRealmResults);
        lvCashOut.setAdapter(reportCashOutAdapter);

        reportCashOutAdapter.notifyDataSetChanged();
        showEmptyListIndicator(cashOutRealmResults.size() <= 0, lvCashOut, viewEmptyCashOut);
    }

    private void openAvailabletoSpendReports(String search){
        RealmResults<AvailabletoSpend> availabletoSpendRealmResults = realm.where(AvailabletoSpend.class).equalTo
                ("batchID", search).findAll();
        availabletoSpendRealmResults.load();

        reportAvailabletoSpendAdapter = new ReportAvailabletoSpendAdapter(context, availabletoSpendRealmResults);
        lvAvailabletoSpend.setAdapter(reportAvailabletoSpendAdapter);

        reportAvailabletoSpendAdapter.notifyDataSetChanged();
        showEmptyListIndicator(availabletoSpendRealmResults.size() <= 0, lvAvailabletoSpend, viewEmptyAvailabletoSpend);
    }

    private void showEmptyListIndicator(boolean show, ListView listView, View emptyIndicator)
    {
        emptyIndicator.setVisibility(show ? View.VISIBLE : View.GONE);
        listView.setVisibility(show ? View.GONE : View.VISIBLE);
    }

}
