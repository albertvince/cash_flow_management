package com.example.xtian.cash_flow_management.DialogFragment;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.xtian.cash_flow_management.Fragments.BatchFragment;
import com.example.xtian.cash_flow_management.Models.Batch;
import com.example.xtian.cash_flow_management.R;

import es.dmoral.toasty.Toasty;
import io.realm.Realm;
import io.realm.RealmResults;

public class DeleteDialogFragment extends DialogFragment {
    private Realm realm;
    private View view;
    private Context context;
    private Button btnConfirm, btnCancel;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.dialog_fragment_confimation, container, false);
        context = view.getContext();
        realm = Realm.getDefaultInstance();

        Bundle bundle = getArguments();
        if (getArguments() != null)
        {
            if (bundle.getString("TAG").equals("DELETE")){
                initializeUI();
            }
        }
        return view;
    }

    private void initializeUI(){
        btnConfirm = view.findViewById(R.id.btnConfirm);
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = getArguments();
                deleteRecord(bundle.getString("BATCHID"));
                dismiss();
                Toasty.success(context, "Record successfully deleted").show();
            }
        });

        btnCancel = view.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    private void deleteRecord(final String search){
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<Batch> result = realm.where(Batch.class).equalTo("uuid", search).findAll();
                result.deleteAllFromRealm();
            }
        });
    }
}
