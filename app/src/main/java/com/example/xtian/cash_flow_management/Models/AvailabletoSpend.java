package com.example.xtian.cash_flow_management.Models;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.xtian.cash_flow_management.Utils.DateTimeHandler;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

public class AvailabletoSpend extends RealmObject implements Parcelable {

    @PrimaryKey
    @Required
    private String uuid;
    private String accountTitle;
    private Double accountAmount;
    private Date dateModified;

    @Index
    private String batchID;
    private int userID;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
    public String getAccountTitle() {
        return accountTitle;
    }

    public void setAccountTitle(String accountTitle) {
        this.accountTitle = accountTitle;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public Date getDateModified() {
        return dateModified;
    }

    public void setDateModified(Date dateModified) {
        this.dateModified = dateModified;
    }

    public Double getAccountAmount() {
        return accountAmount;
    }

    public void setAccountAmount(Double accountAmount) {
        this.accountAmount = accountAmount;
    }

    public String getBatchID() {
        return batchID;
    }

    public void setBatchID(String batchID) {
        this.batchID = batchID;
    }

    public AvailabletoSpend() {

    }


    protected AvailabletoSpend(Parcel in) {
        setUuid(in.readString());
        setAccountTitle(in.readString());
        setAccountAmount(in.readDouble());
        setDateModified(DateTimeHandler.convertStringtoDate(in.readString()));
        setBatchID(in.readString());
    }

    public static final Creator<AvailabletoSpend> CREATOR = new Creator<AvailabletoSpend>() {
        @Override
        public AvailabletoSpend createFromParcel(Parcel in) {
            return new AvailabletoSpend(in);
        }

        @Override
        public AvailabletoSpend[] newArray(int size) {
            return new AvailabletoSpend[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(getUuid());
        parcel.writeString(getAccountTitle());
        parcel.writeDouble(getAccountAmount());
        parcel.writeString(DateTimeHandler.convertDatetoStringDate(getDateModified()));
        parcel.writeString(getBatchID());
    }
}
