package com.example.xtian.cash_flow_management.Fragments;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.example.xtian.cash_flow_management.Activities.CreateBatchActivity;
import com.example.xtian.cash_flow_management.Adapters.CashOutAdapter;
import com.example.xtian.cash_flow_management.DialogFragment.CashOutDialogFragment;
import com.example.xtian.cash_flow_management.Models.CashOut;
import com.example.xtian.cash_flow_management.R;
import com.example.xtian.cash_flow_management.Utils.Debugger;
import com.example.xtian.cash_flow_management.Utils.UserSession;

import java.text.DecimalFormat;
import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

public class CashOutFragment extends Fragment{
    private View view;
    private Context context;
    private Realm realm;
    private RealmResults<CashOut> cashOutRealmResults;
    private RealmChangeListener realmChangeListener;
    private View emptyIndicator;
    public String accountTitle;
    private CashOutAdapter cashOutAdapter;
    private CashOut selectedCashOut;
    private ListView lvRecords;
    private String batchID;
    private TextView tvTotal, tvTotalGST, tvTotalLabel, tvTotalExcludeLabel;
    private LinearLayout linearLayoutTotal, linearLayoutTotalGST;
    private UserSession session;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_cash_out, container,false);
        context = getContext();
        realm = Realm.getDefaultInstance();
        return view;
    }


    public void onStart() {
        super.onStart();
        initializeUI();
        registerForContextMenu(lvRecords);
    }

    public static CashOutFragment newInstance() {
        CashOutFragment fragment = new CashOutFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @SuppressLint("RestrictedApi")
    private void initializeUI() {
        lvRecords = view.findViewById(R.id.lvCashOutRecords);

        emptyIndicator = (View) view.findViewById(R.id.viewEmptyListIndicator);
        batchID = ((CreateBatchActivity)getActivity()).getCurrentBatchID();
        tvTotal = view.findViewById(R.id.tvTotal);
        tvTotalGST = view.findViewById(R.id.tvTotalGST);
        tvTotalLabel = view.findViewById(R.id.tvTotalLabel);
        tvTotalExcludeLabel = view.findViewById(R.id.tvTotalExcludeLabel);
        readRecords(batchID);

        lvRecords.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedCashOut = cashOutAdapter.getItem(position);
                lvRecords.showContextMenu();
            }
        });

        linearLayoutTotal = view.findViewById(R.id.linearLayoutTotal);
        linearLayoutTotal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder myBuilder = new AlertDialog.Builder(getActivity());
                View myView = getLayoutInflater().inflate(R.layout.dialog_total, null);

                TextView tvLabel = (TextView)myView.findViewById(R.id.tvLabel);
                TextView tvTotalAmount = (TextView)myView.findViewById(R.id.tvTotalAmount);
                LinearLayout linearLayoutBackground = myView.findViewById(R.id.linearLayoutTotalBackground);

                tvLabel.setText("Total Excluding GST");
                tvTotalAmount.setText(tvTotal.getText().toString());
                linearLayoutBackground.setBackgroundColor(Color.parseColor("#FFFF4444"));
                myBuilder.setView(myView);
                final AlertDialog dialog = myBuilder.create();
                dialog.show();
            }
        });


        linearLayoutTotalGST = view.findViewById(R.id.linearLayoutTotalGST);
        linearLayoutTotalGST.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder myBuilder = new AlertDialog.Builder(getActivity());
                View myView = getLayoutInflater().inflate(R.layout.dialog_total, null);

                TextView tvLabel = (TextView)myView.findViewById(R.id.tvLabel);
                TextView tvTotalAmount = (TextView)myView.findViewById(R.id.tvTotalAmount);
                LinearLayout linearLayoutBackground = myView.findViewById(R.id.linearLayoutTotalBackground);

                tvLabel.setText("Total GST");
                tvTotalAmount.setText(tvTotalGST.getText().toString());
                linearLayoutBackground.setBackgroundColor(Color.parseColor("#FFCB0001"));
                myBuilder.setView(myView);
                final AlertDialog dialog = myBuilder.create();
                dialog.show();
            }
        });

        if (session.getTaxTerms(context) != null){
            tvTotalLabel.setText("Total " + session.getTaxTerms(context));
            tvTotalExcludeLabel.setText("Total excluding " + session.getTaxTerms(context));
        }
        else {
            tvTotalLabel.setText("Total Tax");
            tvTotalExcludeLabel.setText("Total excluding Tax");
        }


    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        realm.removeAllChangeListeners();
        realm.close();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        menu.add(Menu.NONE, 3, 0, "Edit");
        menu.add(Menu.NONE, 4, 1, "Remove");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getItemId() == 3){
            openCashOutDialogFragment();
        }
        else if (item.getItemId() == 4){
            deleteRecord();
        }
        else {
            return false;
        }
        return true;
    }

    private void openCashOutDialogFragment(){
        String str = selectedCashOut.getUuid();
        Bundle bundle = new Bundle();
        bundle.putString("TAG", str);
        FragmentManager fm = getFragmentManager();
        CashOutDialogFragment cashOutDialogFragment = new CashOutDialogFragment();
        cashOutDialogFragment.setArguments(bundle);
        cashOutDialogFragment.show(fm, "CASHOUT");
    }


    private void readRecords(String search) {
        cashOutRealmResults = realm.where(CashOut.class).equalTo
                ("batchID", search).findAll();
        cashOutRealmResults.load();

        cashOutAdapter = new CashOutAdapter(context, cashOutRealmResults);

        realmChangeListener = new RealmChangeListener() {
            @Override
            public void onChange(Object o) {
                if (cashOutAdapter != null){
                    cashOutAdapter.notifyDataSetChanged();
                }
                showEmptyListIndicator(cashOutRealmResults.size() <= 0);
                readTotalAmount(batchID);
                readTotalGSTAmount(batchID);
            }
        };

        realm.addChangeListener(realmChangeListener);
        lvRecords.setAdapter(cashOutAdapter);

        readTotalAmount(batchID);
        readTotalGSTAmount(batchID);

        showEmptyListIndicator(cashOutRealmResults.size() <= 0);
    }

    private void readTotalAmount(String search) {
        Double totalAmount = 0.0;

        RealmResults<CashOut> cashOutRealmResults = realm.where(CashOut.class).equalTo
                ("batchID", search).findAll();
        ArrayList<CashOut> cashOutArrayList = new ArrayList<>(cashOutRealmResults);

        for (CashOut cashOut : cashOutArrayList) {
            CashOut total = realm.where(CashOut.class).equalTo("accountAmount", cashOut.getAccountAmount()).findFirst();
            totalAmount += total.getAccountAmount();
        }
        DecimalFormat decimalFormat = new DecimalFormat("#,###.00");
        String numberasString = decimalFormat.format(totalAmount);
        if (totalAmount == 0){
            tvTotal.setText(session.getCurrency(context) + "0.00");
        }
        else {
            tvTotal.setText(session.getCurrency(context) + numberasString);
        }
    }

    private void readTotalGSTAmount(String search){
        Double totalGSTAmount = 0.0;

        RealmResults<CashOut> cashOutRealmResults = realm.where(CashOut.class).equalTo ("batchID", search)
                .and().equalTo("hasVAT", 1).findAll();
        ArrayList<CashOut> cashOutArrayList = new ArrayList<>(cashOutRealmResults);

        for (CashOut cashOut : cashOutArrayList) {
            CashOut totalGST = realm.where(CashOut.class).equalTo("VAT", cashOut.getVAT()).findFirst();
            totalGSTAmount += totalGST.getVAT();
        }
        DecimalFormat decimalFormat = new DecimalFormat("#,###.00");
        String numberasString = decimalFormat.format(totalGSTAmount);
        if (totalGSTAmount == 0){
            tvTotalGST.setText(session.getCurrency(context) + "0.00");
        }
        else {
            tvTotalGST.setText(session.getCurrency(context) + numberasString);
        }

    }


    private void showEmptyListIndicator(boolean show)
    {
        emptyIndicator.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    private void deleteRecord() {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<CashOut> result = realm.where(CashOut.class).equalTo("uuid", selectedCashOut.getUuid()).findAll();
                result.deleteAllFromRealm();
                readRecords(batchID);
            }
        });
    }
}
