package com.example.xtian.cash_flow_management.Fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.example.xtian.cash_flow_management.DialogFragment.CurrencyDialogFragment;
import com.example.xtian.cash_flow_management.DialogFragment.TimeFrameDialogFragment;
import com.example.xtian.cash_flow_management.Interface.IDialogCloseInterface;
import com.example.xtian.cash_flow_management.MainActivity;
import com.example.xtian.cash_flow_management.R;
import com.example.xtian.cash_flow_management.Utils.Debugger;
import com.example.xtian.cash_flow_management.Utils.UserSession;

import es.dmoral.toasty.Toasty;
import io.realm.Realm;

public class SettingsFragment extends Fragment implements IDialogCloseInterface.onDismissDialogListener{
    private View view;
    private Context context;
    private LinearLayout lvSync, lvVat, lvTimeFrame, lvCurrency;
    private Switch switchSync, switchVat;
    private boolean isCheckedSync, isCheckedVat;
    private int checkedSync, checkedVat;
    private TextView tvTimeFrame, tvVatType, tvCountry, tvCurrency;
    private Button btnSync;
    private UserSession session;
    private EditText etTax, etTerms;
    private String selectedTimeFrame;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_settings, container, false);
        context = getContext();
        setPageTitle("Settings");
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        initializeUI();
    }

    private void initializeUI() {
        session = new UserSession();
        switchSync = view.findViewById(R.id.switchSync);
        switchVat = view.findViewById(R.id.switchVat);
        lvVat = view.findViewById(R.id.lvVat);
        lvSync = view.findViewById(R.id.lvSync);
        lvTimeFrame = view.findViewById(R.id.lvTimeFrame);
        tvTimeFrame = view.findViewById(R.id.tvTimeFrame);
        tvVatType = view.findViewById(R.id.tvVatType);
        etTax = view.findViewById(R.id.etTax);
        btnSync = view.findViewById(R.id.btnSync);
        lvCurrency = view.findViewById(R.id.lvCurrency);
        etTerms = view.findViewById(R.id.etTerm);
        tvCountry = view.findViewById(R.id.tvCountry);
        tvCurrency = view.findViewById(R.id.tvCurrency);


        lvCurrency.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCurrencyDialog();
            }
        });

        switchVat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    checkedVat = 1;
                    tvVatType.setText("Exclusive");
                }
                else {
                    checkedVat = 0;
                    tvVatType.setText("Inclusive");
                }
            }
        });
        switchSync.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    checkedSync = 1;
                }
                else {
                    checkedSync = 0;
                }
            }
        });
        lvVat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isCheckedVat){
                    switchVat.setChecked(true);
                    isCheckedVat = true;
                    tvVatType.setText("Exclusive");
                }
                else {
                    switchVat.setChecked(false);
                    isCheckedVat = false;
                    tvVatType.setText("Inclusive");
                }
            }
        });
        lvSync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isCheckedSync){
                    switchSync.setChecked(true);
                    isCheckedSync = true;
                }
                else {
                    switchSync.setChecked(false);
                    isCheckedSync = false;
                }
            }
        });
        btnSync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveSettings();
                Toasty.success(context, "Settings successfully configured").show();
            }
        });

        lvTimeFrame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTimeFrameDialog();
            }
        });
        setValues();
        loadSettings();
    }

    private static final String[] TIME_FRAME = new String[]{
            "Weekly", "Monthly", "Quarterly", "Yearly"
    };

    private void openTimeFrameDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Select Time Frame");
        builder.setCancelable(false);
        builder.setSingleChoiceItems(TIME_FRAME, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                selectedTimeFrame = TIME_FRAME[i];
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }

    private void openCurrencyDialog(){
        CurrencyDialogFragment currencyDialogFragment = new CurrencyDialogFragment();
        currencyDialogFragment.setTargetFragment(this, 130);
        currencyDialogFragment.show(getFragmentManager(), "CURRENCY");
    }

    @Override
    public void onDissmissDialog() {
        loadSettings();
    }

    private void saveSettings(){
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt("VAT_AMOUNT", Integer.parseInt(etTax.getText().toString()));
        editor.putInt("AUTO_SYNC", checkedSync);
        editor.putInt("VAT_RATE", checkedVat);
        editor.putString("TIME_FRAME", selectedTimeFrame);
        editor.putString("TAX_TERM", etTerms.getText().toString());
        editor.apply();
    }

    private void setValues(){
        tvTimeFrame.setText(session.getTimeFrame(context));

        if (session.getAutoSync(context) == 1){
            switchSync.setChecked(true);
        }
        else {
            switchSync.setChecked(false);
        }

        if (session.getVatRate(context) == 1){
            switchVat.setChecked(true);
            tvVatType.setText("Exclusive");
        }
        else {
            switchVat.setChecked(false);
            tvVatType.setText("Inclusive");
        }

    }

    public void setPageTitle(String title) {
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(Html.fromHtml("<font color='#ffffff'>"+title+ "</font>"));
    }

    private void loadSettings(){
        if (session != null){
            if (session.getVatRate(context) == 1){
                switchVat.setChecked(true);
                tvVatType.setText("Exclusive");
            }
            else {
                switchVat.setChecked(false);
                tvVatType.setText("Inclusive");
            }
            etTax.setText(String.valueOf(session.getVatAmount(context)));
            Debugger.logD("SOMETHING CHANGE");
            etTerms.setText(session.getTaxTerms(context));

            tvCountry.setText(session.getCountryName(context));
            tvCurrency.setText(session.getCurrency(context));
        }
    }



}
