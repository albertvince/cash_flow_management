package com.example.xtian.cash_flow_management.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.TextView;

import com.example.xtian.cash_flow_management.Models.OpeningBalance;
import com.example.xtian.cash_flow_management.R;
import com.example.xtian.cash_flow_management.Utils.Converter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import io.realm.RealmResults;

public class OpeningBalanceAdapter extends ArrayAdapter<OpeningBalance> implements ListAdapter {

    private final int mResource;
    private int position;
    private Context mContext;
    private List<OpeningBalance> readingList = new ArrayList<>();

    public OpeningBalanceAdapter(@NonNull Context context, RealmResults<OpeningBalance> list) {
        super(context, 0 , list);
        mContext = context;
        mResource = 0;
        readingList = list;
    }

    @Override
    public int getPosition(@Nullable OpeningBalance item)
    {
        int x=0;
        return super.getPosition(item);
    }
    public void setPosition(int _position)
    {
        position = _position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return createViewFromResource(LayoutInflater.from(getContext()), position, convertView, parent, mResource);
    }

    private @NonNull
    View createViewFromResource(@NonNull LayoutInflater inflater, int position,
                                @Nullable View convertView, @NonNull ViewGroup parent, int resource)
    {
        final TextView amount;

        OpeningBalance openingBalance = readingList.get(position);

        if(convertView == null) {
            convertView= LayoutInflater.from(mContext).inflate(R.layout.fragment_opening_balance, parent, false);
        }

        amount = convertView.findViewById(R.id.tvAmount);

        amount.setText(Converter.DoubleToString(openingBalance.getAmount()));

        return convertView;
    }
}
