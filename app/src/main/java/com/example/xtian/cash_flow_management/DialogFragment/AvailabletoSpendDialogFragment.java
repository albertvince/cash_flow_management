package com.example.xtian.cash_flow_management.DialogFragment;


import android.support.v4.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.xtian.cash_flow_management.Activities.CreateBatchActivity;
import com.example.xtian.cash_flow_management.Models.AvailabletoSpend;
import com.example.xtian.cash_flow_management.Models.CashIn;
import com.example.xtian.cash_flow_management.R;
import com.example.xtian.cash_flow_management.Utils.Converter;
import com.example.xtian.cash_flow_management.Utils.DateTimeHandler;
import com.example.xtian.cash_flow_management.Utils.Debugger;
import com.example.xtian.cash_flow_management.Utils.UserSession;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import es.dmoral.toasty.Toasty;
import io.realm.Realm;
import io.realm.RealmResults;

public class AvailabletoSpendDialogFragment extends DialogFragment {
    private Context context;
    private View view;
    private Realm realm;
    private Button btnSave;
    private EditText etAccountTitle, etAmount;
    private String batchID;
    private String accountTitle;
    private Double accountAmount;
    private Date dateModified;
    private String selectedAvailabletoSpend;
    private LinearLayout linearLayoutVAT, linearLayoutSwitch;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.dialog_fragmen_add_amount, container, false);
        context = view.getContext();
        realm = Realm.getDefaultInstance();

        if (getArguments() != null)
        {
            Bundle bundle = getArguments();
            selectedAvailabletoSpend = bundle.getString("TAG");
            initializeUI();
        }

        return view;
    }

    private void initializeUI() {
        TextView tvFragmentName = view.findViewById(R.id.tvFragmentName);
        tvFragmentName.setText("Available to Spend");
        btnSave = view.findViewById(R.id.btnSave);
        etAccountTitle = view.findViewById(R.id.etAccountTitle);
        etAmount = view.findViewById(R.id.etAmount);
        linearLayoutVAT = view.findViewById(R.id.linearLayoutVAT);
        linearLayoutVAT.setVisibility(View.GONE);
        linearLayoutSwitch  = view.findViewById(R.id.linearLayoutSwitch);
        linearLayoutSwitch.setVisibility(View.GONE);
        AvailabletoSpend availabletoSpend = realm.where(AvailabletoSpend.class).equalTo("uuid", selectedAvailabletoSpend).findFirst();
        if (availabletoSpend != null){
            setRecords(availabletoSpend);
        }

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveToDB();
            }
        });

    }

    private void saveToDB() {
       AvailabletoSpend availabletoSpend = realm.where(AvailabletoSpend.class).contains("uuid", selectedAvailabletoSpend).findFirst();
        realm.beginTransaction();
        if (availabletoSpend == null) {
            AvailabletoSpend newAvailabletoSpend = realm.createObject(AvailabletoSpend.class, UUID.randomUUID().toString());
            setFields(newAvailabletoSpend);
        }
        else {
            setFields(availabletoSpend);
        }
        realm.commitTransaction();
        dismiss();
        Toasty.success(context, "Successfully saved.").show();
    }

    private void setFields(AvailabletoSpend availabletoSpend) {
        setData();
        availabletoSpend.setAccountTitle(accountTitle);
        availabletoSpend.setAccountAmount(accountAmount);
        availabletoSpend.setDateModified(dateModified);
        availabletoSpend.setBatchID(batchID);
        availabletoSpend.setUserID(UserSession.getUserId(context));
    }

    private void setRecords(AvailabletoSpend availabletoSpend){
        etAccountTitle.setText(availabletoSpend.getAccountTitle());
        etAmount.setText(Converter.DoubleToString(availabletoSpend.getAccountAmount()));
    }

    public void errorMessage()
    {
        if (etAmount.getText().toString().matches("") && etAccountTitle.getText().toString().matches("")){
            etAccountTitle.setError("Please enter a valid amount");
            etAccountTitle.requestFocus();
        }else if (etAccountTitle.getText().toString().matches("")){
            etAccountTitle.setError("Please enter a valid data");
            etAccountTitle.requestFocus();
        }else if (etAmount.getText().toString().matches("")){
            etAmount.setError("Please enter a valid amount");
            etAmount.requestFocus();
        } else{
            dismiss();
        }
    }

    private void setData() {
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        calendar.add(Calendar.DATE, 30);

        accountTitle = etAccountTitle.getText().toString();
        accountAmount = Converter.StringToDouble(etAmount.getText().toString());
        batchID = ((CreateBatchActivity)getActivity()).getCurrentBatchID();

        dateModified = date;
    }
}
