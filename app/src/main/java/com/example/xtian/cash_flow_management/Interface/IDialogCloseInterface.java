package com.example.xtian.cash_flow_management.Interface;

import android.os.Bundle;

public interface IDialogCloseInterface {
    void handleDialogClose(String quantity);//or whatever args you want

    void handleInvoiceOptions(String account);

    interface onDataSelectionCompleteListener {
        void onComplete(Bundle bundle);
    }

    interface onInvoiceRevenueCompleteListener{
        void onCompleteInvoiceRevenue(Bundle bundle);
    }

    interface onPayBillsExpenseCompleteListener{
        void onCompletePayBillsExpense(Bundle bundle);
    }

    interface onFilterDialogCompleteListener{
        void onFilterDone(Bundle bundle);
    }
    interface onDismissDialogListener{
        void onDissmissDialog();
    }
}
