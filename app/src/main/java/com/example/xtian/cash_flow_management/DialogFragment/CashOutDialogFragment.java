package com.example.xtian.cash_flow_management.DialogFragment;

import android.support.v4.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.example.xtian.cash_flow_management.Activities.CreateBatchActivity;
import com.example.xtian.cash_flow_management.Fragments.CashInFragment;
import com.example.xtian.cash_flow_management.Fragments.CashOutFragment;
import com.example.xtian.cash_flow_management.Models.CashIn;
import com.example.xtian.cash_flow_management.Models.CashOut;
import com.example.xtian.cash_flow_management.R;
import com.example.xtian.cash_flow_management.Utils.Converter;
import com.example.xtian.cash_flow_management.Utils.DateTimeHandler;
import com.example.xtian.cash_flow_management.Utils.Debugger;
import com.example.xtian.cash_flow_management.Utils.UserSession;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import es.dmoral.toasty.Toasty;
import io.realm.Realm;

public class CashOutDialogFragment extends DialogFragment {
    private Context context;
    private View view;
    private Realm realm;
    private Button btnSave;
    private EditText etAccountTitle, etAmount;
    private TextView tvAmount, tvTax, tvTotal, tvVatType;
    private Switch switchVAT;
    private String batchID;
    private String accountTitle;
    private Double accountAmount;
    private Date dateModified;
    private LinearLayout linearLayoutVAT;
    private String selectedCashOut;
    private UserSession session;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.dialog_fragmen_add_amount, container, false);
        context = view.getContext();
        realm = Realm.getDefaultInstance();

        if (getArguments() != null)
        {
            Bundle bundle = getArguments();
            selectedCashOut = bundle.getString("TAG");
            initializeUI();
        }

        return view;
    }

    private void initializeUI() {
        TextView tvFragmentName = view.findViewById(R.id.tvFragmentName);
        tvFragmentName.setText("Cash Out");
        btnSave = view.findViewById(R.id.btnSave);
        etAccountTitle = view.findViewById(R.id.etAccountTitle);
        etAmount = view.findViewById(R.id.etAmount);
        tvAmount = view.findViewById(R.id.tvAmount);
        tvTotal = view.findViewById(R.id.tvTotal);
        tvTax = view.findViewById(R.id.tvTax);
        tvVatType = view.findViewById(R.id.tvVatType);
        switchVAT = view.findViewById(R.id.switchVAT);
        linearLayoutVAT = view.findViewById(R.id.linearLayoutVAT);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveToDB();
            }
        });

        CashOut cashOut = realm.where(CashOut.class).equalTo("uuid", selectedCashOut).findFirst();
        if (cashOut != null){
            setRecords(cashOut);
        }

        if (switchVAT.isChecked()){
            linearLayoutVAT.setVisibility(View.VISIBLE);
        }
        else {
            linearLayoutVAT.setVisibility(View.GONE);
        }

        switchVAT.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    linearLayoutVAT.setVisibility(View.VISIBLE);
                }
                else {
                    linearLayoutVAT.setVisibility(View.GONE);
                }
            }
        });


        etAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                double amount = Converter.StringToDouble(etAmount.getText().toString());
                double tax;
                double total;
                if (session.getVatRate(context) == 0){
                    tax = (session.getVatAmount(context) / 100.0);
                    double taxamount = amount / (tax + 1);
                    taxamount = taxamount * tax;
                    double inclusiveamount = amount - taxamount;
                    tvAmount.setText(Converter.DoubleToString(inclusiveamount));
                    tvTax.setText(Converter.DoubleToString(taxamount));
                    tvTotal.setText(Converter.DoubleToString(amount));
                    tvVatType.setText("(Inclusive)");
                }
                else {
                    tax = (session.getVatAmount(context) / 100.0) * amount;
                    total = amount + tax;
                    tvAmount.setText(Converter.DoubleToString(amount));
                    tvTax.setText(Converter.DoubleToString(tax));
                    tvTotal.setText(Converter.DoubleToString(total));
                    tvVatType.setText("(Exclusive)");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private void setRecords(CashOut cashOut){
        etAccountTitle.setText(cashOut.getAccountTitle());
        etAmount.setText(Converter.DoubleToString(cashOut.getAccountAmount()));

        tvAmount.setText(etAmount.getText().toString());
        double amount = Converter.StringToDouble(tvAmount.getText().toString());
        double tax = cashOut.getVAT() / amount;
        tax = tax * 100;

        double total = amount + cashOut.getVAT();
        tvTax.setText(Converter.DoubleToString(cashOut.getVAT()));
        tvTotal.setText(Converter.DoubleToString(total));

        if (cashOut.getHasVAT() == 1){
            switchVAT.setChecked(true);
            if (session.getVatRate(context) == 1){
                tvVatType.setText("(Exclusive)");
            }
            else {
                tvVatType.setText("(Inclusive)");
            }
        }
    }


    public void errorMessage()
    {
        if (etAmount.getText().toString().matches("") && etAccountTitle.getText().toString().matches("")){
            etAccountTitle.setError("Please enter a valid amount");
            etAccountTitle.requestFocus();
        }else if (etAccountTitle.getText().toString().matches("")){
            etAccountTitle.setError("Please enter a valid data");
            etAccountTitle.requestFocus();
        }else if (etAmount.getText().toString().matches("")){
            etAmount.setError("Please enter a valid amount");
            etAmount.requestFocus();
        } else{
            dismiss();
        }
    }

    private void saveToDB() {
        CashOut cashOut = realm.where(CashOut.class).contains("uuid", selectedCashOut).findFirst();
        realm.beginTransaction();
        if (cashOut == null) {
            CashOut newCashOut = realm.createObject(CashOut.class, UUID.randomUUID().toString());
            setFields(newCashOut);
        }
        else {
            setFields(cashOut);
        }
        realm.commitTransaction();
        dismiss();
        Toasty.success(context, "Successfully saved.").show();
//        sendReceiver();
    }

    private void setFields(CashOut cashOut) {
        setData();
        cashOut.setAccountTitle(accountTitle);
        cashOut.setDateModified(dateModified);
        cashOut.setBatchID(batchID);
        cashOut.setUserID(UserSession.getUserId(context));
        if (switchVAT.isChecked()){
            cashOut.setHasVAT(1);
            cashOut.setAccountAmount(Converter.StringToDouble(tvAmount.getText().toString()));
            cashOut.setVAT(Converter.StringToDouble(tvTax.getText().toString()));
        }
        else {
            cashOut.setHasVAT(0);
            cashOut.setAccountAmount(accountAmount);
            cashOut.setVAT(0.0);
        }
    }


    private void setData() {
        //Date Today
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        calendar.add(Calendar.DATE, 30);

        accountTitle = etAccountTitle.getText().toString();
        accountAmount = Converter.StringToDouble(etAmount.getText().toString());
        batchID = ((CreateBatchActivity)getActivity()).getCurrentBatchID();

        dateModified = date;
    }


}
