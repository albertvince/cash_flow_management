package com.example.xtian.cash_flow_management.DialogFragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.xtian.cash_flow_management.Fragments.BatchFragment;
import com.example.xtian.cash_flow_management.Fragments.SettingsFragment;
import com.example.xtian.cash_flow_management.R;
import com.example.xtian.cash_flow_management.Utils.Debugger;
import com.example.xtian.cash_flow_management.Utils.UserSession;

public class TimeFrameDialogFragment extends DialogFragment {
    private Context context;
    private View view;
    private LinearLayout weekly, monthly, quarterly, yearly;
    private String selectedTimeFrame;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.dialog_list_row_time_frames, container, false);
        context = view.getContext();
        initializeUI();
        Debugger.printO(getTargetRequestCode());
        return view;
    }

    private void initializeUI(){
        weekly = view.findViewById(R.id.linearLayoutWeekly);
        weekly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedTimeFrame = "Weekly";
                openAlertDialog();
            }
        });

        monthly = view.findViewById(R.id.linearLayoutMonthly);
        monthly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedTimeFrame = "Monthly";
                openAlertDialog();
            }
        });

        quarterly = view.findViewById(R.id.linearLayoutQuarterly);
        quarterly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedTimeFrame = "Quarterly";
                openAlertDialog();
            }
        });

        yearly = view.findViewById(R.id.linearLayoutYearly);
        yearly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedTimeFrame = "Yearly";
                openAlertDialog();
            }
        });
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("TIME_FRAME", selectedTimeFrame);
        editor.apply();

        if (getTargetRequestCode() == 121){

            ((SettingsFragment)getTargetFragment()).onDissmissDialog();
        }
        else if (getTargetRequestCode() == 356){
            ((BatchFragment)getTargetFragment()).onDissmissDialog();
        }
    }

    private void openAlertDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(true);
        builder.setTitle("Change Time Frame");
        builder.setMessage("Changing the time frame will delete all of its data. Do you still want to proceed?");
        builder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dismiss();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
